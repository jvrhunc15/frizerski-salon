
Module.register("rssNoviceCeste", {
	defaults: {
		url: ""
	},
	getScript: function () {
		return ["modules/rssNoviceCeste/js/jquery-3.1.1.min.js","modules/rssNoviceCeste/js/feednami.js"];
	},
	getStyles: function () {
		return ["modules/rssNoviceCeste/css/rss-ceste.css"];
	},
	start: function () {
		Log.info("Starting module: " + this.name);
	},
	getDom: function () {
		let naslov = document.createElement("div");
		naslov.id = "naslov";
		let porocilo = document.createElement("marquee");
		porocilo.behavior = "scroll";
		porocilo.direction = "up";
		porocilo.scrollAmount = 1;
		porocilo.id = "porocilo";

		naslov.appendChild(porocilo);
		return naslov;
	},
	notificationReceived: function (notification, payload) {
		var self = this;

		switch (notification) {
			case "START_RSS_CESTE":

				var url = this.config.url;

				feednami.load(url, function(result) {
					if(result.error) {
						console.log(result.error);
					} else {
						let entries = result.feed.entries;

						let naslov, porocilo;
						naslov = document.getElementById("naslov");
						porocilo = document.getElementById("porocilo");

						naslov.innerHTML = entries[0].title;
						porocilo.innerHTML = entries[0].description;

						naslov.appendChild(porocilo);
					}
				});

				break;
		}
	}
});