
Module.register("navodila_salon", {
	defaults: {
		spol: '',
		dolzina: '',
		barva: '',
		tipkovnica_glas_izbira: 'Izbirate lahko med dvema tipoma vnosa:' +
			'<ul>' +
			'<li>Za tip tipkovnica pritisnite \'T\'</li>' +
			'<li>Za tip glasovni nazdor pritiniste \'G\'</li>' +
			'</ul>',


		// TO SO NAVODILA ZA NADZOROVANJE S POMOČJO TIPKOVNICE
		zacetek: "<p>Ukazi [začetek]:</p>" +
			"<ul>" +
			"<li>'Enter' - začetek programa</li>" +
			"</ul>",
		izbira_spola: "<p>Ukazi [izbira spola]:</p>" +
			"<ul>" +
			"<li>'M' - izbira moškega spola,</li>" +
			"<li>'F' - izbira ženskega spola</li>" +
			"</ul>",
		izbira_dolzine: "<p>Ukazi [izbira dolžine las]:</p>" +
			"<ul>" +
			"<li>'S' - izbira kratkih las,</li>" +
			"<li>'L' - izbira dolgih las</li>" +
			"</ul>",
		izbira_barve: "<p>Ukazi [izbira barve las]:</p>" +
			"<ul>" +
			"<li>'L' - svetla barva las</li>" +
			"<li>'D' - temna barva las</li>" +
			"</ul>",
		ali_zadovoljiva_izbira: "<p>Ukazi [potrditev izbire]:</p>" +
			"<ul>" +
			"<li>'Enter' - z izbiro ste zadovoljni in želite nadaljevati,</li>" +
			"<li>'R' - ponoviti želite izbiro atributov</li>" +
			"</ul>",
		prepoznaj_obraz_nastavitev: "<p> Ob pritisku na ukaz 'Enter' se odpre okno v katerem boste videli kvadrat v velikosti glave in dva manjša kvadrata, ki sta namenjena namestitvi oči." +
			"Prosim, da se postavite v lego, ki se kar se da prilega narisanim kvadratom. Ko boste nameščeni se z očmi zazrite v kamero in šele nato pritisnete ukaz.(tj. prepoznavo obraza)</p>" +
			"<p> Ukazi [zajem slike in prepoznavanje obraza]: </p>" +
			"<ul>" +
			"<li>'Enter' - zagon programa za prikaz obraza nato 'Enter' - zajem slike in prepoznava obraza</li>" +
			"</ul>",
		zajem_slike_napaka: "<p>Prosim, če obraz postavite v začrtane kvadrate!</p>" +
			"<p> Ukazi [napaka pri zajemu slike]:</p>" +
			"<ul>" +
			"<li>'Enter' - zagon programa za prikaz obraza nato 'Enter' - ponoven zajem slike in prepoznava obraza</li>" +
			"</ul>",
		v_bazi_je_slika1: "<p>Prikazana je QR koda s povezavo do vaše slike. </p>" +
			"<ul>" +
			"<li>'Enter' - nadaljevanje na prikazovanje navideznih pričesk</li>" +
			"</ul>",
		v_bazi_ni_slike1: "<p>V spominu nimamo nobene vaše slike.</p>" +
			"<ul>" +
			"<li>'Enter' - nadaljevanje na izbiro navidezne pričeske</li>" +
			"</ul>",
		prepoznaj_obraz_napaka: "<p>Ni bilo mogoče zaznati obraza.</p>" +
			"<ul>" +
			"<li>'R' - ponoven zajem slike</li>" +
			"<li>'Enter' - nadaljujemo na prikazovanje navideznih pričesk</li></ul>",
		prikazi_pricesko_navodilo: "Navodilo za izbiro pričeske: " +
			"<ul>" +
			"<li>Na zajeto sliko se vam bodo začele prikazovati pričeske na vaši zajeti fotografiji.</li>" +
			"<li>S puščicama -> in <- se premikate med njimi.</li>" +
			"<li>Ko vam je pričeska všeč jo izbere s pritiskom na \'Enter\'.</li>" +
			"</ul> Za nadeljevanje pritistnite 'Enter'",
		priceska_izbrana: "Uspešno ste izbrali pričesko. " +
			"<ul>" +
			"<li>'R' - če s pričesko niste zadovoljni izbiranje ponovite.</li>" +
			"<li>'Z' - če vam  vaša zajeta slika ni všeč, jo ponovno zajamete.</li>" +
			"<li>'N' - ponoven izbor tipa pričesk.</li>" +
			"<li>'Enter' - vse je v redu. Nadaljujemo na striženje.</li>" +
			"</ul>",
		cakaj_na_konec_strizenja: "<p>Počakajte na konec striženja.</p>" +
			"<ul>" +
			"<li>'Enter' - konec striženja</li>" +
			"</ul> ",
		strizenje_koncano_potrditev: "<p>Ali je striženje res končano?</p>" +
			"<ul>" +
			"<li>'Y' - Da, striženje je končano</li>" +
			"<li>'N'- Ne, striženje želim še nadaljevati.</li>" +
			"</ul>",
		strizenje_koncano: "<p>Striženje uspešno zaključeno. Preden izvršite naslednji ukaz se vzravnajte, saj bomo ponovno zajeli vašo fotografijo</p>" +
			"<ul>" +
			"<li>'Enter' - zajem fotografije po striženju</li>" +
			"</ul>",
		slika_after_zajeta: "<p>V levem zgornjem kotu je prikazana slika po striženju.</p>" +
			"<ul>" +
			"<li>'R' - ponoven zajem slike po striženju</li>" +
			"<li>'Enter' - slika je v redu, nadaljujemo na kreiranje animacij</li>" +
			"</ul>",
		ponovno_zajem_after: "<p>Ponovno se vzravnajte.</p>" +
			"<ul>" +
			"<li>'Enter' - ponoven zajem slike</li>" +
			"</ul>",
		gifi_kreirani: "Gif datoteke so uspešno kreirane. " +
			"<ul>" +
			"<li>V nadaljevanju vam bodo vse prikazane in zraven dodane QR kode s katerimi jih lahko pridobite.</li>" +
			"<li>Med njimi se sprehajate s puščicama -> in <-.</li>" +
			"<li>Ko želite mesto ogleda zapustiti pritisnite tipko 'Esc'</li>" +
			"</ul> " +
			"Pritisnite 'Enter' za nadaljevanje in ogled animacij.",
		ocenjevanje: "Prosim ocenite delo frizerja. " +
			"<ul>" +
			"<li>Med ocenami se premikate s puščicama -> in <-</li>" +
			"<li>Za potrditev kliknite 'Enter'</li>" +
			"</ul>",
		zakljucek: "Hvala za zaupanje.",



		// TO SO NAVODILA ZA GLASOVNO NADZOROVANJE
		zacetek_glas: "<p>Ukazi [začetek]:</p>" +
			"<ul>" +
			"<li>'začni' - začetek programa</li>" +
			"</ul>",
		izbira_spola_glas: "<p>Ukazi [izbira spola]:</p>" +
			"<ul>" +
			"<li>'moški' - izbira moškega spola,</li>" +
			"<li>'ženski' - izbira ženskega spola</li>" +
			"</ul>",
		izbira_dolzine_glas: "<p>Ukazi [izbira dolžine las]:</p>" +
			"<ul>" +
			"<li>'kratki' - izbira kratkih las,</li>" +
			"<li>'dolgi' - izbira dolgih las</li>" +
			"</ul>",
		izbira_barve_glas: "<p>Ukazi [izbira barve las]:</p>" +
			"<ul>" +
			"<li>'svetli' - svetla barva las</li>" +
			"<li>'temni' - temna barva las</li>" +
			"</ul>",
		ali_zadovoljiva_izbira_glas: "<p>Ukazi [potrditev izbire]:</p>" +
			"<ul>" +
			"<li>'naprej' - z izbiro ste zadovoljni in želite nadaljevati,</li>" +
			"<li>'ponovi' - ponoviti želite izbiro atributov</li>" +
			"</ul>",
		prepoznaj_obraz_nastavitev_glas: "<p> Ob izreženem ukazu 'slika' se odpre okno v katerem boste videli kvadrat v velikosti glave in dva manjša kvadrata, ki sta namenjena namestitvi oči." +
			"Prosim, da se postavite v lego, ki se kar se da prilega narisanim kvadratom. Ko boste nameščeni se z očmi zazrite v kamero in šele nato pritisnete 'Enter'.(tj. prepoznavo obraza)</p>" +
			"<p> Ukazi [zajem slike in prepoznavanje obraza]: </p>" +
			"<ul>" +
			"<li>'slika' - zagon programa za prikaz obraza nato pritisk 'Enter' - zajem slike in prepoznava obraza</li>" +
			"</ul>",
		zajem_slike_napaka_glas: "Napaka pri zajemu slike. Prosimo če se vzravnate in poskusite ponovno. Ko boste pripravljeni, recite \'slika\'",
		v_bazi_je_slika1_glas: "<p>Prikazana je QR koda s povezavo do vaše slike. </p>" +
			"<ul>" +
			"<li>'naprej' - nadaljevanje na prikazovanje navideznih pričesk</li>" +
			"</ul>",
		v_bazi_ni_slike1_glas: "<p>V spominu nimamo nobene vaše slike.</p>" +
			"<ul>" +
			"<li>'naprej' - nadaljevanje na izbiro navidezne pričeske</li>" +
			"</ul>",
		prepoznaj_obraz_napaka_glas: "<p>Ni bilo mogoče zaznati obraza.</p>" +
			"<ul>" +
			"<li>'ponovi' - ponoven zajem slike</li>" +
			"<li>'naprej' - nadaljujemo na prikazovanje navideznih pričesk</li>" +
			"</ul>",
		prikazi_pricesko_navodilo_glas: "<p>Navodilo za izbiro pričeske: </p>" +
			"<ul>" +
			"<li>Na zajeto sliko se vam bodo začele prikazovati navidezne pričeske.</li>" +
			"<li>Z ukazoma \'levo\' in \'desno\' se premikate med njimi.</li>" +
			"<li>Ko vam je pričeska všeč jo izbere z ukazom \'izberi\'.</li>" +
			"</ul> Če razumete navodila recite 'naprej' in pričnite izbiro",
		priceska_izbrana_glas: "<p>Uspešno ste izbrali pričesko. </p>" +
			"<ul>" +
			"<li>'ponovi' - če s pričesko niste zadovoljni izbiranje ponovite.</li>" +
			"<li>'slika' - če vam  vaša zajeta slika ni všeč, jo ponovno zajamete.</li>" +
			"<li>'tip' - ponoven izbor tipa pričesk.</li>" +
			"<li>'naprej' - vse je v redu. Nadaljujemo na striženje.</li>" +
			"</ul>",
		cakaj_na_konec_strizenja_glas: "<p>Počakajte na konec striženja.</p>" +
			"<ul>" +
			"<li>'konec' - konec striženja</li>" +
			"</ul> ",
		strizenje_koncano_potrditev_glas: "<p>Ali je striženje res končano?</p>" +
			"<ul>" +
			"<li>'da' - striženje je končano</li>" +
			"<li>'ne'- striženje želim še nadaljevati.</li>" +
			"</ul>",
		strizenje_koncano_glas: "<p>Striženje uspešno zaključeno. Preden izvršite naslednji ukaz se vzravnajte, saj bomo ponovno zajeli vašo fotografijo</p>" +
			"<ul>" +
			"<li>'slika' - zajem fotografije po striženju</li>" +
			"</ul>",
		slika_after_zajeta_glas: "<p>V levem zgornjem kotu je prikazana slika po striženju.</p>" +
			"<ul>" +
			"<li>'ponovi' - ponoven zajem slike po striženju</li>" +
			"<li>'naprej' - slika je v redu, nadaljujemo na kreiranje animacij</li>" +
			"</ul>",
		ponovno_zajem_after_glas: "<p>Ponovno se vzravnajte.</p>" +
			"<ul>" +
			"<li>'slika' - ponoven zajem slike</li>" +
			"</ul>",
		gifi_kreirani_glas: "<p>Gif datoteke so uspešno kreirane.</p>" +
			"<ul>" +
			"<li>V nadaljevanju vam bodo vse prikazane in zraven dodane QR kode s katerimi jih lahko pridobite.</li>" +
			"<li>Med njimi se sprehajate z ukazoma \'desno\' in \'levo\'</li>" +
			"<li>Ko želite mesto ogleda zapustiti recite 'izhod'</li>" +
			"</ul> " +
			"Recite 'naprej' za nadaljevanje in ogled animacij.",
		ocenjevanje_glas: "Prosim ocenite delo frizerja. " +
			"<ul>" +
			"<li>Oceno izbere tako, da izgovorite število od 0 do 10</li>" +
			"<li>Ko je ocena ustrezna recite 'potrdi'</li>" +
			"</ul>",

	},

	start: function() {

	},

	getStyles: function() {
		return ["frizerski_salon.css"];
	},

	getDom: function() {
		let element;
		element = document.createElement("div");
		element.id = "navodilo";
		element.style.fontFamily = 'Arial';
		element.style.width = "1000px";

		let spolElement, dolzinaElement, barvaElement;

		spolElement = document.createElement("div");
		spolElement.innerHTML = "test";
		spolElement.style = "float: left; width: 33.33%;";

		dolzinaElement = document.createElement("div");
		dolzinaElement.innerHTML = "test";
		dolzinaElement.style = "float: left; width: 33.33%;";

		barvaElement = document.createElement("div");
		barvaElement.innerHTML = "test";
		barvaElement.style = "float: left; width: 33.33%;";

		element.appendChild(spolElement);
		element.appendChild(dolzinaElement);
		element.appendChild(barvaElement);
		return element;
	},

	notificationReceived: function(notification, payload, sender) {

		// init spodnjih celic ki bodo prikazovale spol, dolžino in barvo
		let element;
		let spolElement;
		let dolzinaElement;
		let barvaElement;
		let spolTable;
		let dolzinaTable;
		let barvaTable;
		let spolTableCell1;
		let spolTableCell2;
		let dolzinaTableCell1;
		let dolzinaTableCell2;
		let barvaTableCell1;
		let barvaTableCell2;

		spolElement = document.createElement("div");
		spolElement.style = "float: left; width: 33.33%;";
		spolTable = document.createElement("table");
		spolTable.style.width = "85%";
		spolTableCell1 = document.createElement("td");
		spolTableCell2 = document.createElement("td");
		spolTableCell1.style.border = "2px solid white";
		spolTableCell2.style.border = "2px solid white";
		spolTableCell1.id = "male";
		spolTableCell2.id = "female";
		spolTableCell1.innerText = "Moški";
		spolTableCell2.innerText = "Ženski";
		spolTable.appendChild(spolTableCell1);
		spolTable.appendChild(spolTableCell2);
		spolElement.appendChild(spolTable);


		dolzinaElement = document.createElement("div");
		dolzinaElement.style = "float: left; width: 33.33%;";
		dolzinaTable = document.createElement("table");
		dolzinaTable.style.width = "85%";
		dolzinaTableCell1 = document.createElement("td");
		dolzinaTableCell2 = document.createElement("td");
		dolzinaTableCell1.style.border = "2px solid white";
		dolzinaTableCell2.style.border = "2px solid white";
		dolzinaTableCell1.id = "short";
		dolzinaTableCell2.id = "long";
		dolzinaTableCell1.innerText = "Kratki";
		dolzinaTableCell2.innerText = "Dolgi";
		dolzinaTable.appendChild(dolzinaTableCell1);
		dolzinaTable.appendChild(dolzinaTableCell2);
		dolzinaElement.appendChild(dolzinaTable);


		barvaElement = document.createElement("div");
		barvaElement.style = "float: left; width: 33.33%;";
		barvaTable = document.createElement("table");
		barvaTable.style.width = "85%";
		barvaTableCell1 = document.createElement("td");
		barvaTableCell2 = document.createElement("td");
		barvaTableCell1.style.border = "2px solid white";
		barvaTableCell2.style.border = "2px solid white";
		barvaTableCell1.id = "light";
		barvaTableCell2.id = "dark";
		barvaTableCell1.innerText = "Svetli";
		barvaTableCell2.innerText = "Temni";
		barvaTable.appendChild(barvaTableCell1);
		barvaTable.appendChild(barvaTableCell2);
		barvaElement.appendChild(barvaTable);



		switch (notification) {

			// Prikaži izbiro med TIPKOVNICO in GLASOVNIM NAZDOROM
			case "tipkovnica_glas":
				element = document.getElementById("navodilo");
				element.innerHTML = this.defaults.tipkovnica_glas_izbira;
				this.sendNotification("tipkovnica_glas_ok", null);
				break;

			// Prikaži začetno navodilo
			case "zacetek":
				element = document.getElementById("navodilo");
				if(payload.t_g === 'T') {
					element.innerHTML = this.defaults.zacetek;
				} else {
					element.innerHTML = this.defaults.zacetek_glas;
				}
				this.sendNotification("zacetek_ok", null);
				break;

			// Prikaži navodilo za izbiro spola
			case "izberi_spol":
				element = document.getElementById("navodilo");
				if(payload.t_g === 'T') {
					element.innerHTML = this.defaults.izbira_spola;
				} else {
					element.innerHTML = this.defaults.izbira_spola_glas;
				}
				element.appendChild(spolElement);
				element.appendChild(dolzinaElement);
				element.appendChild(barvaElement);
				this.sendNotification("izbira_spola_ok", null);
				break;

			// Prikaži navodila za izbiro dolzine
			case "izberi_dolzino":
				element = document.getElementById("navodilo");

				if(payload.spol === 'M') {
					this.spol = 'M';
				} else {
					this.spol = 'F';
				}

				if(payload.t_g === 'T') {
					element.innerHTML = this.defaults.izbira_dolzine;
				} else {
					element.innerHTML = this.defaults.izbira_dolzine_glas;
				}

				if(this.spol === "M") {
					spolTableCell1.bgColor = "green";
				} else {
					spolTableCell2.bgColor = "green";
				}

				element.appendChild(spolElement);
				element.appendChild(dolzinaElement);
				element.appendChild(barvaElement);

				this.sendNotification("izbira_dolzine_ok", null);
				break;

			// Prikaži navodila/sporočilo za nalaganje podatkov
			case "izberi_barvo":
				element = document.getElementById("navodilo");
				if(payload.dolzina === 'L') {
					this.dolzina = 'L';
				} else {
					this.dolzina = 'S';
				}

				if(payload.t_g === 'T') {
					element.innerHTML = this.defaults.izbira_barve;
				} else {
					element.innerHTML = this.defaults.izbira_barve_glas;
				}

				if(this.spol === "M") {
					spolTableCell1.bgColor = "green";
				} else {
					spolTableCell2.bgColor = "green";
				}

				if(this.dolzina === "S") {
					dolzinaTableCell1.bgColor = "green";
				} else {
					dolzinaTableCell2.bgColor = "green";
				}

				element.appendChild(spolElement);
				element.appendChild(dolzinaElement);
				element.appendChild(barvaElement);
				this.sendNotification("izbira_barve_ok", null);
				break;

			// Prikaži sporočilo, da so bili izbrani vsi atributi (kateri so) in ali je stranka zadovoljena z izbiro)
			case "prikazi_izbiro":
				element = document.getElementById("navodilo");
				console.log("test: " + payload.barva);
				if(payload.barva === 'L') {
					barvaTableCell1.bgColor = "green";
					this.barva = 'L'
				} else {
					barvaTableCell2.bgColor = "green";
					this.barva = 'D'
				}
				let text = "<p>Izbrani so ";

				if(this.barva === 'L') {
					text += 'svetli '
				} else {
					text += 'temni '
				}

				if(this.dolzina === 'L') {
					text += 'dolgi '
				} else {
					text += 'kratki '
				}

				if(this.spol === 'M') {
					text += 'moški '
				} else {
					text += 'ženski '
				}

				text += "lasje.</p>";
				if(payload.t_g === 'T') {
					text += this.defaults.ali_zadovoljiva_izbira;
				} else {
					text += this.defaults.ali_zadovoljiva_izbira_glas;
				}
				element.innerHTML = text;

				if(this.spol === "M") {
					spolTableCell1.bgColor = "green";
				} else {
					spolTableCell2.bgColor = "green";
				}

				if(this.dolzina === "S") {
					dolzinaTableCell1.bgColor = "green";
				} else {
					dolzinaTableCell2.bgColor = "green";
				}

				element.appendChild(spolElement);
				element.appendChild(dolzinaElement);
				element.appendChild(barvaElement);
				this.sendNotification("prikazi_izbiro_ok", null);
				break;

			// Izpiši navodilo kako naj se stranka postavi pred ogledalo
			case "prepoznaj_obraz_nastavitev":
				element = document.getElementById("navodilo");

				if(payload.t_g === 'T') {
					element.innerHTML = this.defaults.prepoznaj_obraz_nastavitev;
				} else {
					element.innerHTML = this.defaults.prepoznaj_obraz_nastavitev_glas;
				}
				this.sendNotification("prepoznaj_obraz_nastavitev_ok", null);
				break;

			// Potrebna ponovna postavitev pred ogledalo
			case "ponovno_zajemi_sliko":
				element = document.getElementById("navodilo");
				if(payload.t_g === 'T') {
					element.innerHTML = this.defaults.zajem_slike_napaka;
				} else {
					element.innerHTML = this.defaults.zajem_slike_napaka_glas;
				}
				this.sendNotification("prepoznaj_obraz_nastavitev_ok", null);
				break;

			// Izpiši vprašanje koga je prepoznala in navodilo za naprej
			case "preveri_identiteto":
				element = document.getElementById("navodilo");

				if(payload.t_g === 'T') {
					if(payload.name === 'unknown') {
						element.innerHTML = this.defaults.prepoznaj_obraz_napaka;
						this.sendNotification("ni_prepoznalo_obraza_ok", null);
					} else if(payload.name === 'napaka') {
						element.innerHTML = this.defaults.prepoznaj_obraz_napaka;
						this.sendNotification("ni_prepoznalo_obraza_ok", null);
					} else {
						element.innerHTML = "<p>Pozdravljen " + payload.name + "! Ali želiš dostop do svoje zadnje slike v tem frizerskem studiju?</p>" +
							"<ul>" +
							"<li>'y' - Da, želim dostop do svoje zadnje slike.</li>" +
							"<li>'n' - Ne želim dostopa do svoje zadnje slike.</li>" +
							"</ul>";
						this.sendNotification("preveri_identiteto_potrditev", {name: payload.name.split(" ")[0], surname: payload.name.split(" ")[1]});
					}
				} else {
					if(payload.name === 'unknown') {
						element.innerHTML = this.defaults.prepoznaj_obraz_napaka_glas;
						this.sendNotification("ni_prepoznalo_obraza_ok", null);
					} else if(payload.name === 'napaka') {
						element.innerHTML = this.defaults.prepoznaj_obraz_napaka_glas;
						this.sendNotification("ni_prepoznalo_obraza_ok", null);
					} else {
						element.innerHTML = "Pozdravljen " + payload.name + "! Ali želiš dostop do svoje zadnje slike v tem frizerskem studiju? (Ja/Ne)";
						this.sendNotification("preveri_identiteto_potrditev", {name: payload.name.split(" ")[0], surname: payload.name.split(" ")[1]});
					}
				}

				break;

			// Izpisi navodilo za prikazovanje pričesk na zajeti sliki
			case "prikazovanje_pričesk_navodilo":
				element = document.getElementById("navodilo");
				if(payload.t_g === 'T') {
					element.innerHTML = this.defaults.prikazi_pricesko_navodilo;
				} else {
					element.innerHTML = this.defaults.prikazi_pricesko_navodilo_glas;
				}
				this.sendNotification("prikazovanje_pričesk_navodilo_ok", null);
				break;

			// V bazi je slika navodilo
			case "v_bazi_slika":
				element = document.getElementById("navodilo");
				if(payload.t_g === 'T') {
					if(payload.file === null) {
						element.innerHTML = this.defaults.v_bazi_ni_slike1;
						this.sendNotification("ni_slike_v_bazi", null);
					} else {
						element.innerHTML = this.defaults.v_bazi_je_slika1;
						this.sendNotification("je_slika_v_bazi", {file: payload.file});
					}
				} else {
					if(payload.file === null) {
						element.innerHTML = this.defaults.v_bazi_ni_slike1_glas;
						this.sendNotification("ni_slike_v_bazi", null);
					} else {
						console.log("PAYLOAD FILE: " + payload.file);
						element.innerHTML = this.defaults.v_bazi_je_slika1_glas;
						this.sendNotification("je_slika_v_bazi", {file: payload.file});
					}
				}
				break;

			case "skrij_qr_naprej_navodilo":
				this.sendNotification("skrij_qr_naprej", null);
				break;

			// Začni gradnjo pričesk
			case "prikazovanje_pricesk":
				element = document.getElementById("navodilo");
				element.innerHTML = "";
				this.sendNotification("gradnja_priceske", null);
				break;

			// Začni prikaz pričesk
			case "zacni_prikaz_pricesk":
				element = document.getElementById("navodilo");
				element.innerHTML = "";
				this.sendNotification("zacni_prikaz_pricesk_ok", {files: payload.files, image_folder: payload.image_folder});
				break;

			// Sporočilo o izbrani pričeski
			case "priceska_izbrana":
				element = document.getElementById("navodilo");
				if(payload.t_g === 'T') {
					element.innerHTML = this.defaults.priceska_izbrana;
				} else {
					element.innerHTML = this.defaults.priceska_izbrana_glas;
				}
				this.sendNotification("priceska_izbrana_ok", null);
				break;

			// Cakaj na konec striženja
			case "priceska_ustrezna":
				element = document.getElementById("navodilo");
				if(payload.t_g === 'T') {
					element.innerHTML = this.defaults.cakaj_na_konec_strizenja;
				} else {
					element.innerHTML = this.defaults.cakaj_na_konec_strizenja_glas;
				}
				this.sendNotification("cakaj_na_frizerja", null);
				break;

			// Striženje je končano, potrditev
			case "strizenje_koncano_potrditev":
				element = document.getElementById("navodilo");
				if(payload.t_g === 'T') {
					element.innerHTML = this.defaults.strizenje_koncano_potrditev;
				} else {
					element.innerHTML = this.defaults.strizenje_koncano_potrditev_glas;
				}
				this.sendNotification("strizenje_koncano_potrditev_ok", null);
				break;

			// Strizenje koncano, cakaj na zajem slike
			case "strizenje_koncano":
				element = document.getElementById("navodilo");
				if(payload.t_g === 'T') {
					element.innerHTML = this.defaults.strizenje_koncano;
				} else {
					element.innerHTML = this.defaults.strizenje_koncano_glas;
				}
				this.sendNotification("strizenje_koncano_ok", null);
				break;

			// Slika after je zajeta
			case "slika_after":
				element = document.getElementById("navodilo");
				if(payload.t_g === 'T') {
					element.innerHTML = this.defaults.slika_after_zajeta;
				} else {
					element.innerHTML = this.defaults.slika_after_zajeta_glas;
				}
				this.sendNotification("slika_after_ok", null);
				break;

			// Ponovno zajemamo sliko after
			case "ponovno_zajem_after":
				element = document.getElementById("navodilo");
				if(payload.t_g === 'T') {
					element.innerHTML = this.defaults.ponovno_zajem_after;
				} else {
					element.innerHTML = this.defaults.ponovno_zajem_after_glas;
				}
				this.sendNotification("strizenje_koncano_ok", null);
				break;

			// Gid datoteke so kreirano
			case "gifi_narejeni_navodilo":
				element = document.getElementById("navodilo");
				if(payload.t_g === 'T') {
					element.innerHTML = this.defaults.gifi_kreirani;
				} else {
					element.innerHTML = this.defaults.gifi_kreirani_glas;
				}
				this.sendNotification("gifi_narejeni_navodilo_ok", null);
				break;

			// Prikazi prej navidezno gif
			case "prej_navidezna_gif":
				element = document.getElementById("navodilo");
				element.innerHTML = "";
				this.sendNotification("prej_navidezna_gif_prikazi", null);
				break;

			// Prikazi prej navidezno potem gif
			case "prej_navidezna_potem_gif":
				element = document.getElementById("navodilo");
				element.innerHTML = "";
				this.sendNotification("prej_navidezna_potem_gif_prikazi", null);
				break;

			// Prikazi prej potem gif
			case "prej_potem_gif":
				element = document.getElementById("navodilo");
				element.innerHTML = "";
				this.sendNotification("prej_potem_gif_prikazi", null);
				break;

			// Prikazi navodilo za ocenjevanje
			case "ocenjevanje":
				element = document.getElementById("navodilo");
				if(payload.t_g === 'T') {
					element.innerHTML = this.defaults.ocenjevanje;
				} else {
					element.innerHTML = this.defaults.ocenjevanje_glas;
				}
				this.sendNotification("ocenjevanje_ok", null);
				break;

			case "zakljucek":
				element = document.getElementById("navodilo");
				element.innerHTML = this.defaults.zakljucek;
				this.sendNotification("konec", null);
				break;
		}
	},

	socketNotificationReceived: function(notification, payload) {

	}
});