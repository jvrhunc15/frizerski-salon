
Module.register("QR_code", {
	defaults: {

	},

	start: function() {

	},

	getStyles: function() {
		return [];
	},

	getDom: function() {
		let element;
		element = document.createElement("img");
		element.id = "qr_code";
		element.innerHTML = "KODA";


		return element;
	},

	notificationReceived: function(notification, payload, sender) {

	},

	socketNotificationReceived: function(notification, payload) {

	}
});