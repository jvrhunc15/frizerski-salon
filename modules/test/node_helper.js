const path = require("path");
const spawn = require("child_process").spawn;

var NodeHelper = require("node_helper");

module.exports = NodeHelper.create({
	start: function() {
		console.log("Starting helper for module: " + this.name);
	},
	/**
	 * Pridobivanje obvestila in obravnjavanje le-tega.
	 * @param notification tip obvestila, ki ga prejmemo
	 * @param payload dodatni podatki, ki jih prejmemo z obvestilom
	 */
	socketNotificationReceived: function(notification, payload) {

		switch (notification) {
			case "START":
				this.listen();
				break;
			case "REPEAT":
				this.listen();
				break;
			case "test":
				console.log("TEST JE USPEŠEN!");
				this.sendSocketNotification("testU", "OK!");
				break;
		}
	},
	/**
	 * Poklicemo python skripto in poslusamo za izrecenimi ukazi
	 * 	- ko je skripta pripravljena za poslušanje naredi "beep"
	 */
	listen: function () {
		const pyProcess = this.runPythonScript();

		pyProcess.stdout.on('data', (data) => {

			console.log("HELPER >> Getting data from python code");
			console.log("HELPER >> Data: " + data.toString());

			// Pridobimo podatke iz python skripte in jih posljemo v uveljavljanje
			this.sendSocketNotification("UKAZ", data.toString());
		});
	},
	/**
	 * Nastavitve za klicanje python skripte
	 * 	- nastavitve poti do datoteke
	 * 	- vrnemo proces s potjo do datoteke
	 */
	runPythonScript: function () {
		let pyPath = path.join(__dirname, "py", "test.py");
		return spawn('python3', [pyPath]);
	}
});