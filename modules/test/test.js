
Module.register("test",{
	/**
	 * Default nastavitve za modul.
	 * 	- ukazi: množica ukazov, ki jih je možno izvesti
	 * 	- moduleShown: vsi možni moduli in indikator, ki pove ali so prikazani ali ne(true/false)
	 */
	defaults: {
		ukazi: {
			napaka: "napaka",
			zapri: "zapri",
			clock: "ura",
			clock2: "uro",
			clock3: "uri",
			vreme: "vreme",
			prazniki: "prazniki",
			novice: "novice",
			novice2: "novica",
			novicaNazaj: "novica nazaj",
			noviceNazaj: "novice nazaj",
			novicaNaprej: "novica naprej",
			noviceNaprej: "novice naprej",
			noviceCeste: "promet",
			koledar: "koledar",
			koledar2: "kolendar"
		},
		moduleShown: {
			prazniki: false,
			novice: false,
			ura: false,
			promet: false,
			vreme: false,
			koledar: false
		}
	},
	start: function() {

		// Prvotni ukaz (da ne izpišemo undefined)
		this.textToWrite = "Prosimo povejte svoj ukaz!";

		// Stevec za novice
		//	- ko ga enkrat povečamo za ena se nebo več klicalo zaganjanje
		//	- da jih nemoremo večkrat zagnat, kar pripelje do preskakovanja po dve
		this.noviceCounter = 0;

	},
	/**
	 * Pridobimo css za modul
	 * @returns {string[]} css file
	 */
	getStyles: function() {
		return ["test.css"];

	},
	/**
	 * Začetno stanje izgleda modula
	 * @returns {HTMLDivElement}
	 */
	getDom: function() {
		let element, subElement;

		element = document.createElement("div");
		element.innerHTML = this.textToWrite;
		element.id = "ukaz";
		subElement = document.createElement("div");
		subElement.innerHTML = "Poslušam...";
		subElement.id = "poslusam";
		subElement.className = "blink_me";
		element.appendChild(subElement);
		return element;
	},
	notificationReceived: function(notification, payload, sender) {
		if (notification === "DOM_OBJECTS_CREATED") {

			// Poslji sporocilo helperju naj začne poslušati
			console.log("TEST >> Start listening");
			this.sendSocketNotification("START", null);

		}
	},
	socketNotificationReceived: function(notification, payload) {

		// Elementa, ki jih bomo spreminjali
		let elem, subElement;

		let textToWrite = "";

		if (notification === "UKAZ") {

			elem = document.getElementById("ukaz");
			subElement = document.getElementById("poslusam");

			// Odstranimo nepotrebne znake iz pridobljenega ukaza
			payload = payload.trim();

			// UKAZ = NAPAKA - NEPREPOZNAN
			if(payload === this.config.ukazi.napaka) {
				console.log("NAPAKA!");
				textToWrite = "Napaka! Prosimo poskusite ponovno!";
			// UKAZ = ZAPRI
			} else if(payload === this.config.ukazi.zapri) {

				// Zapremo vse module
				this.sendNotification("MODULE_TOGGLE", {hide: ["clock", "MMM-3Day-Forecast", "calendar", "rssNoviceSiol", "rssNoviceCeste", "calendar_monthly"], show: [], toogle:[]});
				this.config.moduleShown.ura = false;
				this.config.moduleShown.vreme = false;
				this.config.moduleShown.koledar = false;
				this.config.moduleShown.novice = false;
				this.config.moduleShown.promet = false;
				this.config.moduleShown.prazniki = false;

				textToWrite = "Zaprti vsi moduli!"

			// UKAZ = URA
			} else if(payload === this.config.ukazi.clock
				|| payload === this.config.ukazi.clock2
				|| payload === this.config.ukazi.clock3){

				if(this.config.moduleShown.ura) {
					// Ura je prikazana --> skrijemo uro
					this.config.moduleShown.ura = false;
					this.sendNotification("MODULE_TOGGLE", {hide: ["clock"], show: [], toogle:[]});
					textToWrite = "Ura skrita!"
				} else {
					// Ura je skirta --> prikažemo uro
					this.config.moduleShown.ura = true;
					this.sendNotification("MODULE_TOGGLE", {hide: [], show: ["clock"], toogle:[]});
					textToWrite = "Ura prikazana!"
				}



			// UKAZ = VREME
			} else if(payload === this.config.ukazi.vreme) {

				if(this.config.moduleShown.vreme) {
					// Vreme je prikazano --> skrijemo vreme
					this.config.moduleShown.vreme = false;
					this.sendNotification("MODULE_TOGGLE", {hide: ["MMM-3Day-Forecast"], show: [], toogle:[]});
					textToWrite = "Vreme skrito!"
				} else {
					// Vreme je skrito --> preverimo lokacijo --> prikažemo na ustrezni lokaciji
					this.config.moduleShown.vreme = true;
					this.sendNotification("MODULE_TOGGLE", {hide: [], show: ["MMM-3Day-Forecast"], toogle:[]});
					textToWrite = "Vreme prikazano!"
				}

			// UKAZ = PRAZNIKI
			} else if(payload === this.config.ukazi.prazniki) {

				if(this.config.moduleShown.prazniki) {
					this.config.moduleShown.prazniki = false;
					this.sendNotification("MODULE_TOGGLE", {hide: ["calendar"], show: [], toogle:[]});
					textToWrite = "Prazniki skriti!"
				} else {
					this.config.moduleShown.prazniki = true;
					this.sendNotification("MODULE_TOGGLE", {hide: [], show: ["calendar"], toogle:[]});
					textToWrite = "Prazniki prikazani!"
				}

			// UKAZ = NOVICE
			} else if(payload === this.config.ukazi.novice
				|| payload === this.config.ukazi.novice2) {

				if(this.config.moduleShown.novice) {
					this.config.moduleShown.novice = false;
					this.sendNotification("MODULE_TOGGLE", {hide: ["rssNoviceSiol"], show: [], toogle:[]});
					textToWrite = "Novice skrite!"
				} else {
					if(this.config.moduleShown.promet) {
						this.config.moduleShown.promet = false;
						this.sendNotification("MODULE_TOGGLE", {hide: ["rssNoviceCeste"], show: [], toogle:[]});
					}
					this.config.moduleShown.novice = true;
					this.sendNotification("MODULE_TOGGLE", {hide: [], show: ["rssNoviceSiol"], toogle:[]});
					textToWrite = "Novice prikazana!"
				}

				// Preverimo ali gre za prvi zagon novic
				if(this.noviceCounter === 0) {
					this.noviceCounter++;
					this.sendNotification("START_RSS_SIOL", null);
				}
			// UKAZ NOVICA NAZAJ
			} else if(payload === this.config.ukazi.novicaNazaj
				|| payload === this.config.ukazi.noviceNazaj) {
				textToWrite = "Novica prestavljena nazaj!"
				this.sendNotification("RSS_BACK", null);
			// UKAZ NOVICA NAPREJ
			} else if(payload === this.config.ukazi.novicaNaprej
				|| payload === this.config.ukazi.noviceNaprej) {
				textToWrite = "Novica prestavljena naprej!"
				this.sendNotification("RSS_FORWARD", null);
			// UKAZ PROMET
			} else if(payload === this.config.ukazi.noviceCeste) {

				if(this.config.moduleShown.promet) {
					this.config.moduleShown.promet = false;
					this.sendNotification("MODULE_TOGGLE", {hide: ["rssNoviceCeste"], show: [], toogle:[]});
					textToWrite = "Prometne novice skrite!"
				} else {
					if(this.config.moduleShown.novice) {
						this.config.moduleShown.novice = false;
						this.sendNotification("MODULE_TOGGLE", {hide: ["rssNoviceSiol"], show: [], toogle:[]});
					}
					textToWrite = "Prometne novice prikazane!";
					this.config.moduleShown.promet = true;
					this.sendNotification("MODULE_TOGGLE", {hide: [], show: ["rssNoviceCeste"], toogle:[]});
				}

				this.sendNotification("START_RSS_CESTE", null);
			// UKAZ KOLEDAR
			} else if(payload === this.config.ukazi.koledar) {
				if(this.config.moduleShown.koledar) {
					textToWrite = "Koledar skrit!";
					this.config.moduleShown.koledar = false;
					this.sendNotification("MODULE_TOGGLE", {hide: ["calendar_monthly"], show: [], toogle:[]});
				} else {
					textToWrite = "Koledar prikazan!";
					this.config.moduleShown.koledar = true;
					this.sendNotification("MODULE_TOGGLE", {hide: [], show: ["calendar_monthly"], toogle:[]});
				}
			// NEPRPOZNAN UKAZ
			} else {
				textToWrite = "Ukaza ni bilo mogoče prepoznati, prosim ponovite..";
			}

			elem.innerHTML = textToWrite;
			subElement.innerText = "Poslušam...";
			elem.appendChild(subElement);
			this.sendSocketNotification("REPEAT", null);
		} else if(notification === "test") {
			console.log("TESTETESTSTS");
		}
	}
});