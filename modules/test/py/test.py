import speech_recognition as sr

frequency = 2500  # Set Frequency To 2500 Hertz
duration = 1000  # Set Duration To 1000 ms == 1 second

recognizer = sr.Recognizer()
microphone = sr.Microphone(device_index=0)

if not isinstance(recognizer, sr.Recognizer):
    raise TypeError("`recognizer` must be `Recognizer` instance")

if not isinstance(microphone, sr.Microphone):
    raise TypeError("`microphone` must be `Microphone` instance")

# Izpise vse mikrofone
# for index, name in enumerate(microphone.list_microphone_names()):
#    print("Microphone with name \"{1}\" found for `Microphone(device_index={0})`".format(index, name))

recognizer.dynamic_energy_threshold = False
recognizer.energy_threshold = 3000
recognizer.pause_threshold = 0.8

with microphone as source:
    recognizer.adjust_for_ambient_noise(source, duration=1)

    audio = recognizer.listen(source)

    try:
        speech_text = recognizer.recognize_google(audio, language="sl-SI")
        print(speech_text)
    except sr.UnknownValueError:
        print("napaka")
    except sr.RequestError as e:
        print("Napaka. {0}".format(e))
