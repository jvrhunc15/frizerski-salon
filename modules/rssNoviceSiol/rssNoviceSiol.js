
Module.register("rssNoviceSiol", {
	defaults: {
		url: "",
		entriesCounter: 0,
		positionChanged: false
	},
	getScripts: function() {
		return ["modules/rssNoviceSiol/js/jquery-3.1.1.min.js","modules/rssNoviceSiol/js/feednami.js"];
	},
	getStyles: function() {
		return ["modules/rssNoviceSiol/css/rss-feed.css"];
	},
	start: function() {
		Log.info("Starting module: " + this.name);
	},
	getDom: function () {

		var novica = document.createElement("div");
		novica.id = "novica";

		var title = document.createElement("div");
		title.id = "title";

		var image = document.createElement("div");
		image.id = "image";

		var opis = document.createElement("div");
		opis.id = "opis";

		novica.appendChild(title);
		novica.appendChild(image);
		novica.appendChild(opis);
		return novica;
	},
	notificationReceived: function (notification, payload) {
		var self = this;

		switch (notification) {
			case "START_RSS_SIOL":

				var url = this.config.url;
				var counter = 0;

				const TIMEOUT = 1000;
				const TIME_BETWEEN_CHANGE = 30;

				feednami.load(url,function(result){
					if(result.error) {
						console.log(result.error);
					} else {
						// Get entries
						var entries = result.feed.entries;

						let novica, title, image, opis;
						novica = document.getElementById("novica");
						title = document.getElementById("title");
						image = document.getElementById("image");
						opis = document.getElementById("opis");

						setInterval(function () {

							if(counter === 0) {
								self.posodobiNovico(entries, self.config.entriesCounter, novica, title, image, opis);
							} else if(counter % TIME_BETWEEN_CHANGE === 0) {
								self.config.entriesCounter++;
								self.posodobiNovico(entries, self.config.entriesCounter, novica, title, image, opis);
							}

							if(self.config.positionChanged) {
								self.config.positionChanged = false;

								if(self.config.entriesCounter === -1) {
									self.config.entriesCounter = entries.length - 1;
								}

								if(self.config.entriesCounter === entries.length) {
									self.config.entriesCounter = 0;
								}

								counter = 0;
								self.posodobiNovico(entries, self.config.entriesCounter, novica, title, image, opis);
							}

							counter++;

							// Ce pridemo do konca, nastavimo na prvo novico
							if(self.config.entriesCounter === entries.length) {
								self.config.entriesCounter = 0;
							}

						}, TIMEOUT);
					}
				});
				break;
			case "RSS_BACK":
				this.config.entriesCounter--;
				this.config.positionChanged = true;
				break;
			case "RSS_FORWARD":
				this.config.entriesCounter++;
				this.config.positionChanged = true;
				break;
		}
	},
	posodobiNovico: function (entries, entriesCounter, novica, title, image, opis) {

		let description = entries[entriesCounter].description;
		let imTest = description.substring(0, description.indexOf('>') + 1);
		let opTest = description.substring(description.indexOf('>') + 1, description.length);

		title.innerHTML = entries[entriesCounter].title;
		image.innerHTML = imTest;
		opis.innerText = opTest;

		novica.appendChild(title);
		novica.appendChild(image);
		novica.appendChild(opis);
	}
});
