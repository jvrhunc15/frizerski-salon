const path = require("path");
const spawn = require("child_process").spawn;

const NodeHelper = require("node_helper");

const {PythonShell} = require('python-shell');

module.exports = NodeHelper.create({
	start: function() {
		console.log("Starting helper for module: " + this.name);
	},

	socketNotificationReceived: function(notification, payload) {

		switch (notification) {

			// Klicemo zajem slike, ki naredi sliko '__dirname/py/image/'name'.jpg'
			case "zajemi_sliko":
				this.take_a_photo(payload.name);
				break;

			// Prestejemo stevilo datotek v 'modules/frizerski_salon/py/image'
			case "count_files":
				this.count_files();
				break;

			// Klicemo prepoznavo obraza, ki prepozna osebo na sliki
			// '__dirname/py/image/'image_name'.jpg'
			case "zacni_prepoznavo_obraza":
				console.log("Prepoznava obraza za sliko: "+ payload.image_name);
				this.face_recognition(payload.image_name);
				break;

			// Precekiramo ali obstaja kaka slika za trenutno prepozano osebo
			case "ali_obstaja_slika":
				this.ali_je_slika(payload.dir_name);
				break;

			// Klicemo gradnjo navideznih pričesk
			case "zgradi_priceske":
				this.zgradi_priceske(payload.image, payload.spol, payload.dolzina, payload.barva);
				break;

			// Klicemo ker zelimo vidit listo vseh slik z navideznimi frizurami
			case "images_with_haircuts_list":
				this.list_files(payload.image_folder);
				break;

			// Zajamemo sliko po striženju
			case "zajemi_sliko_potem":
				this.take_a_photo_after(payload.name, payload.stevec);
				break;

			// Kreiramo GIF animacije
			case "kreiraj_animacije":
				this.kreiraj_animacije(payload.stevilka_stranke, payload.image_before_name, payload.image_after_name, payload.navidezna_slika, payload.mapa_navidezne);
				break;

			// Shranimo sliko po striženju v bazo znanj posameznega uporabnika (/last_knwon_photo/{Name_Surnema}/{slika_id}.jpg)
			case "shrani_sliko_after":
				this.shrani_sliko_after(payload.stranka, payload.slika);
				break;

			// Kreiramo QR Kodo za sliko zadnjo
			case "create_image_qr":
				this.kreiraj_Qr_kodo(payload.file_name, payload.stranka_name);
				break;

			// Kreiramo QR kodo za GIfe
			case "kreiraj_qr_gifi":
				this.kreiraj_qr_gif(payload.gif_folder);
				break;

			// TUKI NAPREJ SO UKAZI ZA SPEECH RECOGNITION

			case "poslusaj_zacetek":
				this.poslusaj(payload.expected, payload.next);
				break;

			case "poslusaj_spol":
				this.poslusaj(payload.expected, payload.next);
				break;

			case "poslusaj_dolzina":
				this.poslusaj(payload.expected, payload.next);
				break;

			case "poslusaj_barvo":
				this.poslusaj(payload.expected, payload.next);
				break;

			case "poslusaj_zadovoljiva_izbira":
				this.poslusaj(payload.expected, payload.next);
				break;

			case "poslusaj_zajem_slike":
				this.poslusaj(payload.expected, payload.next);
				break;

			case "poslusaj_potrditev_QR":
				this.poslusaj(payload.expected, payload.next);
				break;

			case "poslusaj_ni_slike":
				this.poslusaj(payload.expected, payload.next);
				break;

			case "poslusaj_prepoznava_napaka":
				this.poslusaj(payload.expected, payload.next);
				break;

			case "poslusaj_skrij_QR":
				this.poslusaj(payload.expected, payload.next);
				break;

			case "poslusaj_prikaz_pricesk":
				this.poslusaj(payload.expected, payload.next);
				break;

			case "poslusaj_izbrana_priceska":
				this.poslusaj(payload.expected, payload.next);
				break;

			case "poslusaj_izbira_priceske":
				this.poslusaj(payload.expected, payload.next);
				break;

			case "poslusaj_konec_strizenja":
				this.poslusaj(payload.expected, payload.next);
				break;

			case "poslusaj_konec_strizenja_potrditev":
				this.poslusaj(payload.expected, payload.next);
				break;

			case "poslusaj_slika_potem":
				this.poslusaj(payload.expected, payload.next);
				break;

			case "poslusaj_potrditev_after_slika":
				this.poslusaj(payload.expected, payload.next);
				break;

			case "poslusaj_prikaz_prvage_gifa":
				this.poslusaj(payload.expected, payload.next);
				break;

			case "poslusaj_prvi_gif":
				this.poslusaj(payload.expected, payload.next);
				break;

			case "poslusaj_drugi_gif":
				this.poslusaj(payload.expected, payload.next);
				break;

			case "poslusaj_tretji_gif":
				this.poslusaj(payload.expected, payload.next);
				break;

			case "poslusaj_sprememba_stars":
				this.poslusaj(payload.expected, payload.next);
				break;

		}

	},

	/**
	 * Poslusaj kaj govoreci pove in preveri če je rekel nekaj kar je pričakovano blo {expected} ter presumeri na {next_step}
	 *
	 * @param expected
	 */
	poslusaj: function(expected, next_step) {
		const self = this;
		let pyPath = path.join(__dirname, "py", "speech_recognition_1.py");

		let najdena_beseda = false;
		let beseda = "";
		PythonShell.run(pyPath, { mode: 'text',
				args: [__dirname + "/py/speak_up.png"]},
			function (err, results) {
				if(err) throw err;
				let besede = results[0].split(" ");
				console.log(besede);
				for(let i = 0; i < besede.length; i++) {
					if(self.vsebujeObjekt(besede[i], expected)) {
						console.log("Pričakovana najdena beseda: " + besede[i]);
						najdena_beseda = true;
						beseda = besede[i];
						break;
					}
				}

				if(najdena_beseda) {
					self.sendSocketNotification(next_step, {izbrana_akcija : beseda});
				}

				if(!najdena_beseda) {
					self.poslusaj(expected, next_step);
				}
			});
	},

	vsebujeObjekt: function(obj, list) {
		let i;
		for (i = 0; i < list.length; i++) {
			if (list[i] === obj) {
				return true;
			}
		}
		return false;
	},

	/**
	 * Funkcija pokliče python skripto create_customer_image.py in sliko shrani v
	 * direktorij 'modules/frizerski_salon/py/image'
	 *
	 * @param image_name
	 */
	take_a_photo: function (image_name) {
		const self = this;
		let pyPath = path.join(__dirname, "py", "create_customer_image_video.py");

		PythonShell.run(pyPath, { mode: 'text',
				args: [ image_name,
					__dirname + "/py",
					__dirname + '/py/image/' + image_name + ".jpg",
					__dirname + "/py/haarcascade_frontalface_default.xml",
					__dirname + "/py/haarcascade_eye.xml",]},
			function (err, results) {
				if(err) throw err;
				if(results[0] === 'taken') {
					self.sendSocketNotification("slika_zajeta_ok", {image_name: image_name + ".jpg"});
				}
			});
	},

	/**
	 * Funkcija presteje stevilo datotek v direktoriju 'modules/frizerski_salon/py/image'
	 */
	count_files: function () {
		const self = this;
		const fs = require('fs');
		const dir = __dirname + "/py/image";

		fs.readdir(dir, (err, files) => {
			self.sendSocketNotification("count_files_ok", {number_files: files.length})
		});
	},

	/**
	 * Funkcija za sliko 'modules/frizerski_salon/py/image/'image_name'.jpg'
	 * naredi prepoznavo obraza
	 *
	 * @param image_name
	 */
	face_recognition: function (image_name) {
		const self = this;
		let pyPath = path.join(__dirname, "py", "face_recognition_image.py");

		PythonShell.run(pyPath, { mode: 'text',
			args: [__dirname + "/py/image/" + image_name,
				   __dirname + "/py/dataset",
				   __dirname + "/py/haarcascade_frontalface_default.xml"]},
			function (err, results) {
			if(err) throw err;

			if(results != null) {
				// Result od 0 je vedno najbolj leva oseba na sliki, najbolj desna je n
				let result_split = results[0].split(',');

				if(result_split.length > 1) {
					let name_surname = result_split[0];
					let name = name_surname.split('_')[0];
					let surname = name_surname.split('_')[1];
					let probability = result_split[1];

					self.sendSocketNotification("preveri_identiteto", {full_name: name + " " + surname, probability: probability});
				} else {
					self.sendSocketNotification("preveri_identiteto", {full_name: "unknown", probability: 0});
				}
			}
		});
	},

	/**
	 * Precekiramo ali za osebo dirname (npr. Jernej_Vrhunc) obstaja slika kaka
	 *
	 * @param dir_name
	 */
	ali_je_slika: function(dir_name) {
		const self = this;
		const fs = require('fs');
		const dir = __dirname + "/py/last_known_photos/" + dir_name;

		if(fs.existsSync(dir)) {
			fs.readdir(dir, (err, files) => {
				if(files.length > 0) {
					let pricakovana = files.length;
					let file;
					for (i = 0; i < files.length; i++) {
						if(parseInt(files[i].split('.')[0]) === pricakovana) {
							file = files[i];
						}
					}
					self.sendSocketNotification("ali_slika_obstaja", {file: file});
				} else {
					self.sendSocketNotification("ali_slika_obstaja", {file: null})
				}

			});
		} else {
			self.sendSocketNotification("ali_slika_obstaja", {file: null})
		}
	},

	/**
	 * Funkcija za izbrano mnozico frizur (glede na spol, dolzino in barvo) zgradi
	 * slike, ki vsebujejo navidezne pričeske in vrne številko mape v 'image_with_haircuts'
	 *
	 * @param image_name
	 * @param spol
	 * @param dolzina
	 * @param barva
	 */
	zgradi_priceske: function (image_name, spol, dolzina, barva) {
		const self = this;
		let pyPath = path.join(__dirname, "py", "add_hairstyle_to_image.py");

		PythonShell.run(pyPath, { mode: 'text',
			args: [__dirname + "/py/image/" + image_name,
				   image_name.replace('.jpg', ''),
				   spol,
				   dolzina,
				   barva,
				   __dirname + "/py/haarcascade_frontalface_default.xml",
				   __dirname + "/py/haarcascade_eye.xml",
				   __dirname + "/py/haircuts",
				   __dirname + "/py/resized_haircuts",
				   __dirname + "/py/image_with_haircuts"]},
			function(err, results) {
				if(err) throw err;
				self.sendSocketNotification("priceske_nalozene", {image_folder: results[0]})
		});
	},

	/**
	 * List vseh datotek v podmapi 'folder' direktorija /py/image_with_haircuts/
	 *
	 * @param folder
	 */
	list_files: function (folder) {
		const fs = require('fs');
		let files = fs.readdirSync(__dirname + "/py/image_with_haircuts/" + folder);
		this.sendSocketNotification("images_with_haircuts_list_ok", {files: files, image_folder: folder});
	},

	/**
	 * Kreiranje slike po friziranja
	 *
	 * @param image_name
	 * @param stevec
	 */
	take_a_photo_after: function (image_name, stevec) {
		const self = this;
		let pyPath = path.join(__dirname, "py", "create_customer_image_after.py");

		PythonShell.run(pyPath, { mode: 'text',
				args: [ image_name + "_" + stevec,
					__dirname + "/py",
					__dirname + '/py/image_after/' + image_name +"_" + stevec + ".jpg",
					__dirname + "/py/haarcascade_frontalface_default.xml",
					__dirname + "/py/haarcascade_eye.xml",]},
			function (err, results) {
				if(err) throw err;
				if(results[0] === 'taken') {
					self.sendSocketNotification("slika_after_zajeta_ok", {image_name: image_name + "_" + stevec + ".jpg"});
				}
			});
	},

	/**
	 * Shranimo sliko after (kreiramo tudi direktorij če ga še ni za stranko)
	 *
	 * @param stranka
	 * @param slika
	 */
	shrani_sliko_after: function(stranka, slika) {
		const fs = require('fs');
		let path_to_dir = __dirname + "/py/last_known_photos/" + stranka;

		let file_number;
		// Če ni direktorij ga ustvarimo
		if(!fs.existsSync(path_to_dir)) {
			fs.mkdir(path_to_dir);
			file_number = 1;
		} else {
			let files = fs.readdirSync(__dirname + "/py/last_known_photos/" + stranka);
			file_number = files.length + 1;
		}

		let file_name = file_number + ".jpg";

		let src = __dirname + "/py/image_after/" + slika;
		let dest = __dirname + "/py/last_known_photos/" + stranka + "/" + file_name;

		fs.copyFile(src, dest, (err) => {
			if(err) throw err;
			console.log("Last known photo " + file_name + " copied to 'last_known_photos'");
		});
	},

	/**
	 * Poklicemo python skripto, ki kreira 3 gif datoteke: prej vs potem, prej vs. navidezna in prej vs. navidezna vs. potem
	 *
	 * @param stevilka_stranke
	 * @param before_image
	 * @param after_image
	 * @param navidezna_image
	 * @param mapa_navidezne
	 */
	kreiraj_animacije: function (stevilka_stranke, before_image, after_image, navidezna_image, mapa_navidezne) {
		const self = this;
		let pyPath = path.join(__dirname, "py", "create_animation.py");

		PythonShell.run(pyPath, { mode: 'text',
				args: [ stevilka_stranke,
					__dirname + "/py/animation",
					__dirname + '/py/image',
					__dirname + '/py/image_after',
					__dirname + '/py/image_with_haircuts',
					before_image,
					after_image,
					navidezna_image,
					__dirname + "/py/Hack-BoldItalic.ttf"]},
			function (err, results) {
				if(err) throw err;
				console.log("RESULTS: " + results);
				if(results[0] === 'gifs_created') {
					self.sendSocketNotification("gifi_narejeni", {gif_folder: results[1]});
				}
			});
	},

	/**
	 * Kreiramo QR kodo ki kaže na url: naša_aplikacija_url/image/{stranka_name}/{image_name}
	 *
	 * @param image_name
	 * @param stranka_name
	 */
	kreiraj_Qr_kodo: function (image_name, stranka_name) {
		const self = this;
		let pyPath = path.join(__dirname, "py", "qr_code.py");
		const fs = require('fs');

		let savePath = path.join(__dirname, "..", "..", "js", "images");

		if(!fs.existsSync(savePath + "/" + stranka_name)) {
			fs.mkdirSync(savePath + "/" + stranka_name);
		}

		let src = __dirname + "/py/last_known_photos/" + stranka_name + "/" + image_name;
		let dest = savePath + "/" + stranka_name + "/" + image_name;

		fs.copyFile(src, dest, (err) => {
			if(err) throw err;
			console.log("Last known photo " + image_name + " copied to images");
		});

		let qrCOdesSave = __dirname + "/py/qr_codes";

		PythonShell.run(pyPath, { mode: 'text',
				args: [image_name, stranka_name, qrCOdesSave]},
			function (err, results) {
				if(err) throw err;

				self.sendSocketNotification("prikazi_qr_kodo", {qr_code_image: results[0]});

			});
	},

	/**
	 * Kreiramo QR kodo za vse gife, ki so v mapi {gif_folder}
	 *
	 * @param gif_folder
	 */
	kreiraj_qr_gif: function (gif_folder) {
		const self = this;
		const fs = require('fs');

		console.log(__dirname);
		let gif_folder_path = path.join(__dirname, "py", "animation", gif_folder);

		let pyPath = path.join(__dirname, "py", "qr_code_anime.py");
		let saveFolder = __dirname + "/py/qr_codes_anime";

		if(!fs.existsSync(saveFolder + "/" + gif_folder)) {
			fs.mkdir(saveFolder + "/" + gif_folder);
		}


		let src1 = gif_folder_path + "/prej_navidezna.gif";
		let src2 = gif_folder_path + "/prej_navidezna_potem.gif";
		let src3 = gif_folder_path + "/prej_potem.gif";

		let dest_path = path.join(__dirname, "..", "..", "js", "animations");
		if(!fs.existsSync(dest_path + "/" + gif_folder)) {
			fs.mkdir(dest_path + "/" + gif_folder);
		}
		let dest = path.join(__dirname, "..", "..", "js", "animations", gif_folder);

		fs.copyFile(src1, dest + "/prej_navidezna.gif", (err) => {
			if(err) throw err;
			console.log("Gif 1 transfered to js/animations");
		});
		fs.copyFile(src2, dest + "/prej_navidezna_potem.gif", (err) => {
			if(err) throw err;
			console.log("Gif 2 transfered to js/animations");
		});
		fs.copyFile(src3, dest + "/prej_potem.gif", (err) => {
			if(err) throw err;
			console.log("Gif 3 transfered to js/animations");
		});

		PythonShell.run(pyPath, { mode: 'text',
				args: [gif_folder,
					gif_folder_path, saveFolder]},
			function (err, results) {
				if(err) throw err;
				if(results[0] === "qr_gif_created") {
					self.sendSocketNotification("prikazovanje_gifov", {gif_folder: results[1]})
				}
			});

	}
});