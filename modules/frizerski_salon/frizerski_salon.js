
Module.register("frizerski_salon", {
	defaults: {
		spol: "",
		dolzina: "",
		barva: "",
		stevilka_stranke: 0,
		stranka_name: "",
		mapa_slika_navidezna_priceska: "",
		slika_navidezna_priceska: "",
		slika_prej: "",
		slika_potem: "",
		prva_izbira_podatkov: "",
		prva_zajeta_slika: "",
		stevec: 0,
		ocena: 0,
		tipkovnica_glas: "",
		glas_counter: 0,
		files: [],
		image_folder: "",
        star_counter: 0
	},

	start: function() {
		this.prva_izbira_podatkov = true;
		this.prva_zajeta_slika = true;
		this.stevec = 0;
		this.stranka_name = "";
		this.ocena = 0;
		this.glas_counter = 0;
		this.files = [];
		this.image_folder = "";
        this.star_counter = 0
	},

	getStyles: function() {
		return ["frizerski_salon.css"];
	},

	getDom: function() {
		let element, img;
		element = document.createElement("img");
		element.id = "salon";
		return element;
	},

	notificationReceived: function(notification, payload, sender) {

		const self = this;

		let element;
		switch (notification) {

			// 1. Vsi objekti na front endu uspešno kreirani -> preumseri na izbiro tipa vnosa
			case "DOM_OBJECTS_CREATED":
				// Prikaži začetno navodilo
				this.redirect_to_navodila("tipkovnica_glas", null);
				break;

			// 2. Izbira med tipom vnosa -> T za tipkovnico, G za glasovni zadzor
			case "tipkovnica_glas_ok":
				element = document.getElementsByTagName("BODY")[0];
				element.onkeydown = null;

				element.onkeydown = function (event) {
					// Pritisk T -> izbrana tipkovnica
					if (event.keyCode === 84) {
						self.tipkovnica_glas = 'T';
						self.redirect_to_navodila("zacetek", {t_g : self.tipkovnica_glas});

					// pritisk G -> izbran glasovni nadzor
					} else if(event.keyCode === 71) {
						self.tipkovnica_glas = 'G';
						self.redirect_to_navodila("zacetek", {t_g : self.tipkovnica_glas});
					}
				};
				break;

			// 3.T. Začetno sporočilo je izpisano -> cakaj 'Enter' -> presumeri na izbiro spola
			// 3.G. Začetno sporočilo izpisano -> čakaj da uporabnik reče 'Začni' -> preusmeri na izbiro spola
			case "zacetek_ok":
				if(this.tipkovnica_glas === 'T') {
					element = document.getElementsByTagName("BODY")[0];
					element.onkeydown = null;

					element.onkeydown = function (event) {
						// Pritisk 'Enter'
						if (event.keyCode === 13) {
							self.redirect_to_navodila("izberi_spol", {t_g : self.tipkovnica_glas});
						}
					};
				} else {
					element = document.getElementsByTagName("BODY")[0];
					element.onkeydown = null;

					self.sendSocketNotification("poslusaj_zacetek", {expected: ["začni"], next: "izberi_spol"})
				}

				break;


			// 3. Navodilo za izbiro spola so prikazana -> izberi spol -> preusmeri na izbiranje dolzine
			case "izbira_spola_ok":
				if(this.tipkovnica_glas === 'T') {
					element = document.getElementsByTagName("BODY")[0];
					element.onkeydown = null;

					element.onkeydown = function (event) {

						// Pritisk 'F'
						if (event.keyCode === 70) {
							self.gender_selection('F');
							self.redirect_to_navodila("izberi_dolzino", {spol: 'F', t_g: self.tipkovnica_glas});

							// Pritisk 'M'
						} else if (event.keyCode === 77) {
							self.gender_selection('M');
							self.redirect_to_navodila("izberi_dolzino", {spol: 'M', t_g: self.tipkovnica_glas});
						}
					};
				} else {
					self.sendSocketNotification("poslusaj_spol", {expected: ["moški", "ženski", "ženska"], next: "izberi_dolzino"});
				}

				break;


			// 4. Navodila za izbiro dolzine so prikazana -> izberi dolzino -> preusmeri na izbiro barve
			case "izbira_dolzine_ok":
				if(this.tipkovnica_glas === 'T') {
					element = document.getElementsByTagName("BODY")[0];
					element.onkeydown = null;

					element.onkeydown = function (event) {

						// Pritisk 'L'
						if (event.keyCode === 76) {
							self.length_selection('L');
							self.redirect_to_navodila("izberi_barvo", {dolzina: 'L', t_g: self.tipkovnica_glas});

							// Pritisk 'S'
						} else if (event.keyCode === 83) {
							self.length_selection('S');
							self.redirect_to_navodila("izberi_barvo", {dolzina: 'S', t_g: self.tipkovnica_glas});
						}
					};
				} else {
					self.sendSocketNotification("poslusaj_dolzina", {expected: ["dolge", "dolgi", "kratke", "kratki"], next: "izberi_barvo"});
				}

				break;

			// 5. Navodilo za izbiro barve prikazano -> izberi barvo -> preusmeri na prikaz izbire atributov
			case "izbira_barve_ok":
				if(this.tipkovnica_glas === 'T') {
					element = document.getElementsByTagName("BODY")[0];
					element.onkeydown = null;

					element.onkeydown = function (event) {

						// Pritisk 'L'
						if (event.keyCode === 76) {
							self.color_selection('L');
							self.redirect_to_navodila("prikazi_izbiro", {barva: 'L', t_g: self.tipkovnica_glas});

							// Pritisk 'D'
						} else if (event.keyCode === 68) {
							self.color_selection('D');
							self.redirect_to_navodila("prikazi_izbiro", {barva: 'D', t_g: self.tipkovnica_glas});
						}
					};
				} else {
					self.sendSocketNotification("poslusaj_barvo", {expected: ["temna", "temni", "svetla", "svetli"], next: "prikazi_izbiro"});
				}

				break;

			// 6. Sporočilo,kateri podatki so izbrani prikazano -> cakaj na potrditev ali repeat -> preumseri na repeat ali nadaljevanje (prepoznava obraza ali pogled v slike)
			case "prikazi_izbiro_ok":
				if(this.tipkovnica_glas === 'T') {
					element = document.getElementsByTagName("BODY")[0];
					element.onkeydown = null;

					element.onkeydown = function (event) {
						// Enter pomeni, da smo zadovoljni z izbiro in gremo naprej
						if (event.keyCode === 13) {

							// To pomeni, da smo prvič uspešno izbrali podatke in gremo sedaj naprej na zaznavo obraza
							if(self.prva_izbira_podatkov) {
								self.prva_izbira_podatkov = false;
								self.redirect_to_navodila("prepoznaj_obraz_nastavitev", {t_g: self.tipkovnica_glas});

								// To pomeni, da smo se vrnal na izbiro podatkov iz koraka o izbiri pričeske in gremo sedaj nazaj na izbiro pričeske
							} else {
								self.redirect_to_navodila("prikazovanje_pricesk", {t_g: self.tipkovnica_glas});
							}

						// Pritisk R, kar pomeni da ponovimo postopek
						} else if(event.keyCode === 82) {
							self.prva_izbira_podatkov = true;
							self.redirect_to_navodila("izberi_spol", {t_g: self.tipkovnica_glas});
						}
					};
				} else {
					self.sendSocketNotification("poslusaj_zadovoljiva_izbira", {expected: ["naprej", "ponovi", "ponovitev"], next: "zadovoljiva_izbira"})
				}

				break;

			// 7. Navodilo o postavitvi je izpisano -> čakaj enter -> zajemi sliko (v helperju: 'zajemi_sliko')-> prepoznaj obraz ->
			// preusmeri na potrditev identitete (ali ponovitev slikanja, če se odloči stranka)
			case "prepoznaj_obraz_nastavitev_ok":
				this.sendSocketNotification("count_files", null);

				if(this.tipkovnica_glas === 'T') {
					element = document.getElementsByTagName("BODY")[0];
					element.onkeydown = null;

					element.onkeydown = function (event) {
						if (event.keyCode === 13) {
							// Enter, torej klicemo python skripto, ki naredi sliko osebe in požene na to sliko face recognition
							self.sendSocketNotification("zajemi_sliko", {name: self.stevilka_stranke});
						}
					};
				} else {
					self.sendSocketNotification("poslusaj_zajem_slike", {expected: ["slika", "slikaj"], next: "zajem_slike"})
				}

				break;

			// 8.1. Sporočilo za prikaz QR kode, obraz prepoznan -> Cakamo na y (DA) ali n (NE) prikazovanje QR kode -> Prusmeri na prikaz QR kode ali prikazovanje pričesk
			case "preveri_identiteto_potrditev":
				// Nastavimo ime stranke za kasnejše shranjevanje photo after
				this.stranka_name = payload.name + "_" + payload.surname;

				if(this.tipkovnica_glas === 'T') {
					element = document.getElementsByTagName("BODY")[0];
					element.onkeydown = null;

					element.onkeydown = function (event) {
						// Pritisk 'Y'
						if (event.keyCode === 89) {
							self.sendSocketNotification("ali_obstaja_slika", {dir_name: payload.name + "_" + payload.surname});

							// Pritisk 'N'
						} else if(event.keyCode === 78) {
							self.redirect_to_navodila("prikazovanje_pričesk_navodilo", {t_g: self.tipkovnica_glas});
						}
					};
				} else {
					self.sendSocketNotification("poslusaj_potrditev_QR", {expected: ["ja", "da", "ne"], next: "potrditev_identitete"});
				}
				break;

			// 8.1.1 Prikazano sporočilo, da v bazi ni nobene pričeske -> Čakamo na Enter -> Redirect na prikazovanje pričesk
			case "ni_slike_v_bazi":
				if(this.tipkovnica_glas === 'T') {
					element = document.getElementsByTagName("BODY")[0];
					element.onkeydown = null;

					element.onkeydown = function (event) {
						// cakamo nadaljevanje (Enter)
						if (event.keyCode === 13) {
							self.redirect_to_navodila("prikazovanje_pričesk_navodilo", {t_g: self.tipkovnica_glas});
						}
					};
				} else {
					self.sendSocketNotification("poslusaj_ni_slike", {expected: ["naprej"], next: "prikazovanje_pričesk_navodilo"});
				}

				break;

			// 8.1.2 Prikazano sporočilo, da v bazi je pričeska -> Prikazovanje QR kode -> Čakamo na Enter(skrijemo QR kodo)
			case "je_slika_v_bazi":
				element = document.getElementsByTagName("BODY")[0];
				element.onkeydown = null;

				// Prikazemo qr kodo za zadnjo sliko, ki obstaja v naši bazi za uporabnika
				this.sendSocketNotification("create_image_qr", {file_name: payload.file, stranka_name: this.stranka_name});
				break;

			// 8.1.3 QR koda je prikazana -> čakamo enter -> gremo naprejn na pričeske
			case "skrij_qr_naprej":
				if(this.tipkovnica_glas === 'T') {
					element = document.getElementsByTagName("BODY")[0];
					element.onkeydown = null;

					element.onkeydown = function (event) {
						element = document.getElementsByTagName("BODY")[0];
						element.onkeydown = null;

						// cakamo nadaljevanje (Enter)
						if (event.keyCode === 13) {
							self.sendNotification("MODULE_TOGGLE", {hide: ["QR_code"], show: [], toogle:[]});
							self.redirect_to_navodila("prikazovanje_pričesk_navodilo", {t_g: self.tipkovnica_glas});
						}
					};
				} else {
					self.sendSocketNotification("poslusaj_skrij_QR", {expected: ["naprej"], next: "prikazovanje_pričesk_navodilo"});
				}

				break;

			// 8.2 Sporočilo, da obraz ni bil prepoznan izpisano -> Cakaj 'R' ali 'Enter' -> Preumseri na prepoznavo obraza ali prikazovanje pričesk
			case "ni_prepoznalo_obraza_ok":
				if(this.tipkovnica_glas === 'T') {
					element = document.getElementsByTagName("BODY")[0];
					element.onkeydown = null;

					element.onkeydown = function (event) {

						// Gremo naprej na prikazovanje pričesk (ni prepoznanega obraza) (Enter)
						if (event.keyCode === 13) {
							self.redirect_to_navodila("prikazovanje_pričesk_navodilo", {t_g: self.tipkovnica_glas});

							// Ponovno zajemi sliko in poženi prepoznavo obraza (R)
						} else if(event.keyCode === 82){
							self.redirect_to_navodila("prepoznaj_obraz_nastavitev", {t_g: self.tipkovnica_glas});
						}
					};
				} else {
					self.sendSocketNotification("poslusaj_prepoznava_napaka", {expected: ["naprej", "ponovi", "ponovitev"], next: "napaka_prepoznava"});
				}

				break;

			// 10. Navodilo za prikazovanje pričesk prikazano -> Cakaj enter(pripravljen) -> Preusmeri na prikazovanje pričesk
			case "prikazovanje_pričesk_navodilo_ok":
				if(this.tipkovnica_glas === 'T') {
					element = document.getElementsByTagName("BODY")[0];
					element.onkeydown = null;

					element.onkeydown = function (event) {

						// cakamo pripravljenost (Enter)
						if (event.keyCode === 13) {
							self.redirect_to_navodila("prikazovanje_pricesk", {t_g: self.tipkovnica_glas});
						}
					};
				} else {
					self.sendSocketNotification("poslusaj_prikaz_pricesk", {expected: ["naprej"], next: "prikazovanje_pricesk_ukaz"});
				}

				break;

			// 11. Začni gradnjo pričesk (ni navodila več) -> Preusmeri na prikazovanje pričesk
			case "gradnja_priceske":
				element = document.getElementsByTagName("BODY")[0];
				element.onkeydown = null;

				// Naredim slike z vsemi priceskami
				// (npr za sliko 5.jpg za 5 pricesk:
				// 	- 5_1.jpg
				// 	- 5_2.jpg
				// 	- 5_3.jpg
				// 	- 5_4.jpg
				// 	- 5_5.jpg) -> nalozim v neko vrsto in jih pol po vrsti prikazujem lahko
				this.sendSocketNotification("zgradi_priceske", {image: this.stevilka_stranke + ".jpg", spol: this.spol, dolzina: this.dolzina, barva: this.barva});
				break;

			// 12. Navodila še kr ni -> Redi da prikaže prvo sliko, Pričeske zgrajene, začni prikazovati pričeske
			case "zacni_prikaz_pricesk_ok":
				if(this.tipkovnica_glas === 'T') {
					element = document.getElementsByTagName("BODY")[0];
					element.onkeydown = null;

					// Path do prave mape s slikami
					let path = this.data.path + "/py/image_with_haircuts/" + payload.image_folder;
					// Zbirka vseh slik v mapi (po imenih npr: 8_2.jpg)
					let images = payload.files;

					// counter definiramo na prvo sliko (0)
					let counter = 0;
					// zbrana prva slika
					let selectedImage = images[counter];
					// path do zbrane slike
					let selectedImagePath = path + "/" + selectedImage;

					element.style.backgroundImage = 'url("'+ selectedImagePath +'")';
					element.style.backgroundRepeat = 'no-repeat';
					element.style.backgroundSize = '65% 100%';

					element.onkeydown = function (event) {

						// Izbrana slika (Enter)
						if (event.keyCode === 13) {
							self.slika_navidezna_priceska = selectedImage = images[counter];
							self.mapa_slika_navidezna_priceska = payload.image_folder;
							self.sendNotification("priceska_izbrana", {t_g: self.tipkovnica_glas});
							// Right
						} else if(event.keyCode === 39) {
							counter++;
							if (counter === images.length) {
								counter = 0;
							}

							selectedImage = images[counter];
							selectedImagePath = path + "/" + selectedImage;

							element.style.backgroundImage = 'url("'+ selectedImagePath +'")';
							element.style.backgroundRepeat = 'no-repeat';
							element.style.backgroundSize = '65% 100%';

							// Left
						} else if(event.keyCode === 37) {
							counter--;
							if(counter === -1) {
								counter = images.length - 1;
							}

							selectedImage = images[counter];
							selectedImagePath = path + "/" + selectedImage;

							element.style.backgroundImage = 'url("'+ selectedImagePath +'")';
							element.style.backgroundRepeat = 'no-repeat';
							element.style.backgroundSize = '65% 100%';
						}
					};
				} else {
					element = document.getElementsByTagName("BODY")[0];
					element.onkeydown = null;

					// Path do prave mape s slikami
					let path = this.data.path + "/py/image_with_haircuts/" + payload.image_folder;
					// Zbirka vseh slik v mapi (po imenih npr: 8_2.jpg)
					let images = payload.files;

					// counter globalen
					let counter = this.glas_counter;
					// zbrana prva slika
					let selectedImage = images[counter];
					// path do zbrane slike
					let selectedImagePath = path + "/" + selectedImage;

					element.style.backgroundImage = 'url("'+ selectedImagePath +'")';
					element.style.backgroundRepeat = 'no-repeat';
					element.style.backgroundSize = '65% 100%';

					this.sendSocketNotification("poslusaj_izbira_priceske", {expected: ["desno", "levo", "izberi"], next: "izbira_priceske_ukaz"});
				}
				break;

			// 13. Pričeska izbrana sporočilo prikazano -> Repeat ali potrdi odločitev -> Preusmeri na repeat ali naprej na
			// čakanje frizerja da postriže
			case "priceska_izbrana_ok":
				element = document.getElementsByTagName("BODY")[0];
				element.onkeydown = null;

				let pathImage = this.data.path + "/py/image_with_haircuts/" + this.mapa_slika_navidezna_priceska + "/" + this.slika_navidezna_priceska;

				element.style.backgroundImage = 'url("'+ pathImage +'")';
				element.style.backgroundRepeat = 'no-repeat';
				element.style.backgroundSize = '18% 25%';

				if(this.tipkovnica_glas === 'T') {

					element.onkeydown = function (event) {

						// Enter, smo zadovoljni
						if (event.keyCode === 13) {
							self.redirect_to_navodila("priceska_ustrezna", {t_g: self.tipkovnica_glas});

							// Repeat (R)
						} else if(event.keyCode === 82) {
							element.style.backgroundImage = 'url("none")';
							self.redirect_to_navodila("prikazovanje_pricesk", {t_g: self.tipkovnica_glas});

							// Pritisk 'Z' -> ponovni zajem slike
						} else if(event.keyCode === 90) {
							element.style.backgroundImage = 'url("none")';
							self.redirect_to_navodila("prepoznaj_obraz_nastavitev", {t_g: self.tipkovnica_glas});

							// Pritisk 'N' -> ponovna izbira pričeske
						} else if(event.keyCode === 78) {
							element.style.backgroundImage = 'url("none")';
							self.redirect_to_navodila("izberi_spol", {t_g: self.tipkovnica_glas});
						}
					};
				} else {
					this.sendSocketNotification("poslusaj_izbrana_priceska", {expected: ["ponovi", "slika", "tip", "naprej"], next: "ukaz_po_zajeti"});
				}

				break;

			// 13. Prikazano sporočilo da čakamo na konec striženja -> Čakaj Enter -> Preusmeri na potrditev konca striženja
			case "cakaj_na_frizerja":
				element = document.getElementsByTagName("BODY")[0];
				element.onkeydown = null;

				if(this.tipkovnica_glas === 'T') {
					element.onkeydown = function (event) {
						// Enter, konec striženja
						if (event.keyCode === 13) {
							self.redirect_to_navodila("strizenje_koncano_potrditev", {t_g: self.tipkovnica_glas});
						}
					};
				} else {
					this.sendSocketNotification("poslusaj_konec_strizenja", {expected: ["konec"], next: "ukaz_konec_strizenja"})
				}

				break;

			// 14. Potrditev konca striženja prikazana -> y ali n -> preusmeri na čakanje ali zajem slike after
			case "strizenje_koncano_potrditev_ok":
				if(this.tipkovnica_glas === 'T') {
					element = document.getElementsByTagName("BODY")[0];
					element.onkeydown = null;

					element.onkeydown = function (event) {
						// y konec striženja potridtev
						if (event.keyCode === 89) {
							element.style.backgroundImage = 'url("none")';
							self.redirect_to_navodila("strizenje_koncano", {t_g: self.tipkovnica_glas});
							// n, ni še konec
						} else if(event.keyCode === 78) {
							self.redirect_to_navodila("priceska_ustrezna", {t_g: self.tipkovnica_glas});
						}
					};
				} else {
					this.sendSocketNotification("poslusaj_konec_strizenja_potrditev", {expected: ["ja", "da", "ne"], next: "ukaz_konec_strizenja_potrditev"})
				}


				break;

			// 15. Prikazano sporočilo da zajemamo sliko spet -> čakamo enter -> zajemi sliko after -> preusmeri na prikaz slike ali napako pri slikanju
			case "strizenje_koncano_ok":
				element = document.getElementsByTagName("BODY")[0];
				element.onkeydown = null;

				this.stevec += 1;

				if(this.tipkovnica_glas === 'T') {
					element.onkeydown = function (event) {
						// Enter, konec
						if (event.keyCode === 13) {
							self.sendSocketNotification("zajemi_sliko_potem", {name: self.stevilka_stranke, stevec: self.stevec});
						}
					};
				} else {
					this.sendSocketNotification("poslusaj_slika_potem", {expected: ["slika"], next: "poslusaj_slika_potem_ukaz"})
				}
				break;

			// 16. Slika after je zajeta in se prikaže v zgornjem delu ekrana + navodilo -> kreiranje gifov, preusmeritev na KREIRANI GIFI USPEŠNO
			case "slika_after_ok":
				element = document.getElementsByTagName("BODY")[0];
				element.onkeydown = null;

				element.style.backgroundImage = 'url("'+ this.data.path + "/py/image_after/" + this.slika_potem +'")';
				element.style.backgroundRepeat = 'no-repeat';
				element.style.backgroundSize = '40% 65%';


				if(this.tipkovnica_glas === 'T') {
					element.onkeydown = function (event) {
						// Enter, slika after je uredu
						if (event.keyCode === 13) {
							// Shranimo sliko after za kasnješo uporabo pri naslednjem striženuj..slika after v "Bazi"
							self.sendSocketNotification("shrani_sliko_after", {stranka: self.stranka_name, slika: self.slika_potem});

							// Kreiramo animacije GIF za prikaz
							element.style.backgroundImage = 'url("none")';
							self.sendSocketNotification("kreiraj_animacije", {stevilka_stranke: self.stevilka_stranke,
								image_before_name: self.slika_prej,
								image_after_name: self.slika_potem,
								navidezna_slika: self.slika_navidezna_priceska});
							// Pritisk Z -> ponovno slikanje after
						} else if(event.keyCode === 90) {
							element.style.backgroundImage = 'url("none")';
							self.redirect_to_navodila("ponovno_zajem_after", {t_g: self.tipkovnica_glas});
						}
					};
				} else {
					this.sendSocketNotification("poslusaj_potrditev_after_slika", {expected: ["naprej", "ponovi"], next: "potrditev_slika_potem"})
				}


				break;

			// Navodilo za ogled gifov je izpisano -> Čakamo enter za ogled gifov -> Prikazi prej vs. navidezna gifa
			case "gifi_narejeni_navodilo_ok":
				element = document.getElementsByTagName("BODY")[0];
				element.onkeydown = null;

				if(this.tipkovnica_glas === 'T') {
					element.onkeydown = function (event) {
						// Cakamo na enter da zacnemo prikazovat gife
						if (event.keyCode === 13) {
						    this.star_counter = 0;
							self.redirect_to_navodila("prej_navidezna_gif", {t_g: self.tipkovnica_glas});
						}
					};
				} else {
					this.sendSocketNotification("poslusaj_prikaz_prvage_gifa", {expected: ["naprej"], next: "prikaz_prvage_gifa"})
				}
				break;

			// Prikazano je navodilo za prej navidezna -> prikazemo gif -> cakamo na -> in <- -> preusmerimo na nov gif
			case "prej_navidezna_gif_prikazi":
				element = document.getElementsByTagName("BODY")[0];
				element.onkeydown = null;

				let animePath = this.data.path + "/py/animation/" + this.stevilka_stranke + "/prej_navidezna.gif";
				element.style.backgroundImage = 'url("'+ animePath +'")';
				element.style.backgroundSize = '70% 90%';

				self.sendNotification("MODULE_TOGGLE", {hide: [], show: ["qr_code"], toogle:[]});
				let element1 = document.getElementById("qr_code");
				element1.src = this.data.path +  "/py/qr_codes_anime/" + this.stevilka_stranke + "/prej_navidezna_qr.jpg";
				element1.style = 'width: 150px; height: 150px';

				if(this.tipkovnica_glas === 'T') {
					element.onkeydown = function (event) {

						// Right -> prikazemo prej navidezna potem
						if (event.keyCode === 39) {
							element.style.backgroundImage = 'url("none")';
							self.redirect_to_navodila("prej_navidezna_potem_gif", {t_g: self.tipkovnica_glas});

							// Left -> prikazemo prej potem
						} else if(event.keyCode === 37) {
							element.style.backgroundImage = 'url("none")';
							self.redirect_to_navodila("prej_potem_gif", {t_g: self.tipkovnica_glas});

							// Pritisk ESC -> izhod iz gifov
						} else if(event.keyCode === 27) {
							element.style.backgroundImage = 'url("none")';
							self.sendNotification("MODULE_TOGGLE", {hide: ["qr_code"], show: [], toogle:[]});
							self.redirect_to_navodila("ocenjevanje", {t_g: self.tipkovnica_glas});
						}
					};
				} else {
					this.sendSocketNotification("poslusaj_prvi_gif", {expected: ["desno", "levo", "izhod"], next: "from_prvi_gif"});
				}
				break;

			// Prikazano je navodilo za prej navidezna potem gif -> prikazemo gif -> cakamo na -> in <- -> preusmerimo na nov gif
			case "prej_navidezna_potem_gif_prikazi":
				element = document.getElementsByTagName("BODY")[0];
				element.onkeydown = null;

				let animePath1 = this.data.path + "/py/animation/" + this.stevilka_stranke + "/prej_navidezna_potem.gif";
				element.style.backgroundImage = 'url("'+ animePath1 +'")';

				self.sendNotification("MODULE_TOGGLE", {hide: [], show: ["qr_code"], toogle:[]});
				let element2 = document.getElementById("qr_code");
				element2.src = this.data.path +  "/py/qr_codes_anime/" + this.stevilka_stranke + "/prej_navidezna_potem_qr.jpg";
				element2.style = 'width: 150px; height: 150px';

				if(this.tipkovnica_glas === 'T') {
					element.onkeydown = function (event) {

						// Right -> prikazemo prej potem
						if (event.keyCode === 39) {
							element.style.backgroundImage = 'url("none")';
							self.redirect_to_navodila("prej_potem_gif", {t_g: self.tipkovnica_glas});

							// Left -> prikazemo prej navidezna
						} else if(event.keyCode === 37) {
							element.style.backgroundImage = 'url("none")';
							self.redirect_to_navodila("prej_navidezna_gif", {t_g: self.tipkovnica_glas});

							// Pritisk ESC -> izhod iz gifov
						} else if(event.keyCode === 27) {
							element.style.backgroundImage = 'url("none")';
							self.sendNotification("MODULE_TOGGLE", {hide: ["qr_code"], show: [], toogle:[]});
							self.redirect_to_navodila("ocenjevanje", {t_g: self.tipkovnica_glas});
						}
					};
				} else {
					this.sendSocketNotification("poslusaj_drugi_gif", {expected: ["desno", "levo", "izhod"], next: "from_drugi_gif"});
				}

				break;

			// Prikazano je navodilo prej potem gif -> prikazemo gif -> cakamo na -> in <- -> preusmerimo na nov gif
			case "prej_potem_gif_prikazi":
				element = document.getElementsByTagName("BODY")[0];
				element.onkeydown = null;

				let animePath2 = this.data.path + "/py/animation/" + this.stevilka_stranke + "/prej_potem.gif";
				element.style.backgroundImage = 'url("'+ animePath2 +'")';

				self.sendNotification("MODULE_TOGGLE", {hide: [], show: ["qr_code"], toogle:[]});
				let element3 = document.getElementById("qr_code");
				element3.src = this.data.path +  "/py/qr_codes_anime/" + this.stevilka_stranke + "/prej_potem_qr.jpg";
				element3.style = 'width: 150px; height: 150px';

				if(this.tipkovnica_glas === 'T') {
					element.onkeydown = function (event) {

						// Right -> prikazemo prej navidezna
						if (event.keyCode === 39) {
							element.style.backgroundImage = 'url("none")';
							self.redirect_to_navodila("prej_navidezna_gif", {t_g: self.tipkovnica_glas});

							// Left -> prikazemo prej navidezna potem
						} else if(event.keyCode === 37) {
							element.style.backgroundImage = 'url("none")';
							self.redirect_to_navodila("prej_navidezna_potem_gif", {t_g: self.tipkovnica_glas});

							// Pritisk ESC -> izhod iz gifov
						} else if(event.keyCode === 27) {
							element.style.backgroundImage = 'url("none")';
							self.sendNotification("MODULE_TOGGLE", {hide: ["qr_code"], show: [], toogle:[]});
							self.redirect_to_navodila("ocenjevanje", {t_g: self.tipkovnica_glas});
						}
					};
				} else {
					this.sendSocketNotification("poslusaj_tretji_gif", {expected: ["desno", "levo", "izhod"], next: "from_tretji_gif"});
				}

				break;

			case "ocenjevanje_ok":
				element = document.getElementsByTagName("BODY")[0];
				element.onkeydown = null;

				let star_counter = this.star_counter;
				let stars_path = this.data.path + "stars/";

				let subelement = document.getElementById("salon");
				subelement.src = stars_path + star_counter + "_stars.png";
				subelement.style = " position: fixed; top: 50%; left: 50%; transform: translate(-50%, -50%);";

                if(this.tipkovnica_glas === 'T') {
                    element.onkeydown = function (event) {

						// Right -> prikazemo naslednje zvezdice
                        if (event.keyCode === 39) {
                            if(star_counter < 10) {
                                star_counter += 1;
                            }
                            subelement.src = stars_path + star_counter + "_stars.png";
                            subelement.style = " position: fixed; top: 50%; left: 50%; transform: translate(-50%, -50%);";

						// Left -> prikazemo prej navidezna potem
                        } else if(event.keyCode === 37) {
                            if(star_counter > 0) {
                                star_counter -= 1;
                            }
                            subelement.src = stars_path + star_counter + "_stars.png";
                            subelement.style = " position: fixed; top: 50%; left: 50%; transform: translate(-50%, -50%);";

						// Enter-> redirect na zakljucek
                        } else if(event.keyCode === 13) {
                            this.ocena = star_counter;
                            subelement.src = "";
                            subelement.style = "";
                            self.redirect_to_navodila("zakljucek", {t_g: self.tipkovnica_glas});
                        }
                    };
                } else {
                    this.sendSocketNotification("poslusaj_sprememba_stars",
						{expected: ["nič", "ena", "dva", "tri", "štiri", "pet",
								"šest", "sedem", "osem", "devet", "deset", "potrdi",
								"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"], next: "change_stars"})
                }
				break;

			case "konec":
				element = document.getElementsByTagName("BODY")[0];
				element.onkeydown = null;

				element.onkeydown = function (event) {
					if(event.keyCode === 13) {
						self.spol = "";
						self.dolzina= "";
						self.barva= "";
						self.stevilka_stranke= 0;
						self.stranka_name= "";
						self.mapa_slika_navidezna_priceska= "";
						self.slika_navidezna_priceska= "";
						self.slika_prej= "";
						self.slika_potem= "";
						self.prva_izbira_podatkov= "";
						self.prva_zajeta_slika= "";
						self.stevec= 0;
						self.ocena= 0;
						self.prva_izbira_podatkov = true;
						self.prva_zajeta_slika = true;
						self.redirect_to_navodila("zacetek", null)
					}
				};
				break;

		}

	},

	socketNotificationReceived: function(notification, payload) {

		switch (notification) {

			// Slika je bila uspešno zajeta z imenom 'image_name', pozenimo face recognition
			case "slika_zajeta_ok":
				// Prvic zajemamo sliko --> iz tega sledi prepoznava obraza
				if (this.prva_zajeta_slika) {
					this.prva_zajeta_slika = false;
					this.sendSocketNotification("zacni_prepoznavo_obraza", {image_name: payload.image_name});

					// Klicali smo po novem zajemanju slike in gremo nazaj na prikazovanje pricesk
				} else {
					this.sendNotification("prikazovanje_pricesk", {t_g: this.tipkovnica_glas});
				}

				// Nastavimo kje se nahaja slika prej, za kasnejše primerjanje prej vs potem..
				this.mapa_slika_prej = this.data.path + "/py/image";
				this.slika_prej = payload.image_name;
				break;

			// Prestejemo stevilo file-ov v image folderju, da naredimo za stranko novo sliko z naslednjo stevilko
			case "count_files_ok":
				this.stevilka_stranke = payload.number_files + 1;
				break;

			// Preumserimo na potrditev identitete
			case "preveri_identiteto":
				this.sendNotification("preveri_identiteto", {
					name: payload.full_name,
					probability: payload.probability,
					t_g: this.tipkovnica_glas
				});
				break;

			// SLike z navideznimi pričeskami so uspešno naložene, lahko začnemo prikazovati pričeske
			case "priceske_nalozene":
				this.sendSocketNotification("images_with_haircuts_list", {image_folder: payload.image_folder});
				break;

			// Vrnemo list vseh slik narjenih (z navidezno pričesko)
			case "images_with_haircuts_list_ok":
				this.files = payload.files;
				this.image_folder = payload.image_folder;
				this.sendNotification("zacni_prikaz_pricesk", {
					files: payload.files,
					image_folder: payload.image_folder
				});
				break;

			// Kreiranje slike after
			case "slika_after_zajeta_ok":
				this.slika_potem = payload.image_name;
				this.sendNotification("slika_after", {t_g: this.tipkovnica_glas});
				break;

			// Gifi so bili uspešno kreirani -> sedaj moramo narediti qr kode za njih
			case "gifi_narejeni":
				this.sendSocketNotification("kreiraj_qr_gifi", {gif_folder: payload.gif_folder});
				break;

			// ALi obstaja slika za uporabnika v bazi
			case "ali_slika_obstaja":
				this.sendNotification("v_bazi_slika", {file: payload.file, t_g: this.tipkovnica_glas});
				break;

			// Prikazemo qr kodo
			case "prikazi_qr_kodo":
				this.sendNotification("MODULE_TOGGLE", {hide: [], show: ["QR_code"], toogle: []});
				let element1 = document.getElementById("qr_code");
				let image_name = payload.qr_code_image;
				element1.src = this.data.path + "/py/qr_codes/" + image_name;
				element1.style = 'width: 150px; height: 150px';

				this.sendNotification("skrij_qr_naprej_navodilo", {t_g: this.tipkovnica_glas});
				break;

			// priakzemo navodilo za prikazovanje gifov
			case "prikazovanje_gifov":
				this.sendNotification("gifi_narejeni_navodilo", {t_g: this.tipkovnica_glas});
				break;


			/**
			 * Od tu naprej so Socket Notificationi za GLASOVNI NADZOR
			 */

			// Preusmerimo na izbiro spola (prikaz navodila)
			case "izberi_spol":
				this.sendNotification("izberi_spol", {t_g: this.tipkovnica_glas});
				break;

			// Preusmerimo na izbir dolzine (prikaz navodila)
			case "izberi_dolzino":
				if (payload.izbrana_akcija === 'moški') {
					this.spol = 'M';
					this.sendNotification("izberi_dolzino", {t_g: this.tipkovnica_glas, spol: 'M'});
				} else if (payload.izbrana_akcija === 'ženski' || payload.izbrana_akcija === 'ženska') {
					this.spol = 'F';
					this.sendNotification("izberi_dolzino", {t_g: this.tipkovnica_glas, spol: 'F'});
				}
				break;

			// Preusmerimo na izbiro barve pričeske (prikaz navodil)
			case "izberi_barvo":
				if (payload.izbrana_akcija === 'dolge' || payload.izbrana_akcija === 'dolgi') {
					this.dolzina = 'L';
					this.sendNotification("izberi_barvo", {t_g: this.tipkovnica_glas, dolzina: 'L'});
				} else if (payload.izbrana_akcija === 'kratke' || payload.izbrana_akcija === 'kratki') {
					this.dolzina = 'S';
					this.sendNotification("izberi_barvo", {t_g: this.tipkovnica_glas, dolzina: 'S'});
				}
				break;

			// Prikazi doloceno izbiro in navodilo za naprej ali ponovitev
			case "prikazi_izbiro":
				if (payload.izbrana_akcija === 'temna' || payload.izbrana_akcija === 'temni') {
					this.barva = 'D';
					this.sendNotification("prikazi_izbiro", {t_g: this.tipkovnica_glas, barva: 'D'});
				} else if (payload.izbrana_akcija === 'svetla' || payload.izbrana_akcija === 'svetli') {
					this.barva = 'L';
					this.sendNotification("prikazi_izbiro", {t_g: this.tipkovnica_glas, barva: 'L'});
				}
				break;

			// Dobil smo besedo {payload.izbrana_akcija} -> preusmerimo glede na njo
			case "zadovoljiva_izbira":
				if (payload.izbrana_akcija === 'naprej') {
					if (this.prva_izbira_podatkov) {
						this.prva_izbira_podatkov = false;
						this.sendNotification("prepoznaj_obraz_nastavitev", {t_g: this.tipkovnica_glas});
					} else {
						this.sendNotification("prikazovanje_pricesk", {t_g: this.tipkovnica_glas});
					}
				} else {
					this.sendNotification("izberi_spol", {t_g: this.tipkovnica_glas})
				}
				break;


			// Dobil smo besedo slika
			case "zajem_slike":
				this.sendSocketNotification("zajemi_sliko", {name: this.stevilka_stranke});
				break;

			// Dobimo ja/ne za prikaz QR kode
			case "potrditev_identitete":
				if (payload.izbrana_akcija === 'ja' || payload.izbrana_akcija === 'da') {
					this.sendSocketNotification("ali_obstaja_slika", {dir_name: this.stranka_name})
				} else {
					this.sendNotification("prikazovanje_pričesk_navodilo", {t_g: this.tipkovnica_glas})
				}
				break;

			// Dobimo naprej in gremo na prikazovanje pričesk navodilo
			case "prikazovanje_pričesk_navodilo":
				this.sendNotification("MODULE_TOGGLE", {hide: ["QR_code"], show: [], toogle: []});
				this.sendNotification("prikazovanje_pričesk_navodilo", {t_g: this.tipkovnica_glas});
				break;

			// Dobimo napako o prepoznavi obraza
			case "napaka_prepoznava":
				if (payload.izbrana_akcija === 'naprej') {
					this.sendNotification("prikazovanje_pričesk_navodilo", {t_g: this.tipkovnica_glas});
				} else {
					this.sendNotification("prepoznaj_obraz_nastavitev", {t_g: this.tipkovnica_glas});
				}
				break;

			// Dobimo naprej in gremo prikazovat priceske
			case "prikazovanje_pricesk_ukaz":
				this.sendNotification("prikazovanje_pricesk", {t_g: this.tipkovnica_glas});
				break;

			// Dobimo ukaz desno, levo ali izberi
			case "izbira_priceske_ukaz":
				if (payload.izbrana_akcija === 'desno') {
					this.glas_counter += 1;
					if(this.glas_counter === this.files.length){
						this.glas_counter = 0;
					}
					this.sendNotification("zacni_prikaz_pricesk", {
						files: this.files,
						image_folder: this.image_folder
					});
				} else if (payload.izbrana_akcija === 'levo') {
					this.glas_counter -= 1;
					if(this.glas_counter === -1) {
						this.glas_counter = this.files.length - 1;
					}
					this.sendNotification("zacni_prikaz_pricesk", {
						files: this.files,
						image_folder: this.image_folder
					});
				} else if (payload.izbrana_akcija === 'izberi') {
					let path = this.data.path + "/py/image_with_haircuts/" + this.image_folder;
					let images = this.files;

					this.slika_navidezna_priceska = images[this.glas_counter];
					this.mapa_slika_navidezna_priceska = this.image_folder;

					this.sendNotification("priceska_izbrana", {t_g: this.tipkovnica_glas});
				}
				break;

			// zbrano je kaj nardimo po zajemo after slike (ponovi, tip, slika, naprej)
			case "ukaz_po_zajeti":
				let element_body = document.getElementsByTagName("BODY")[0];
				element_body.onkeydown = null;

				if(payload.izbrana_akcija === "ponovi") {
					element_body.style.backgroundImage = 'url("none")';
					this.redirect_to_navodila("prikazovanje_pricesk", {t_g: this.tipkovnica_glas});
				} else if(payload.izbrana_akcija === "slika") {
					element_body.style.backgroundImage = 'url("none")';
					this.redirect_to_navodila("prepoznaj_obraz_nastavitev", {t_g: this.tipkovnica_glas});
				} else if(payload.izbrana_akcija === "tip") {
					element_body.style.backgroundImage = 'url("none")';
					this.redirect_to_navodila("izberi_spol", {t_g: this.tipkovnica_glas});
				} else if(payload.izbrana_akcija === "naprej") {
					this.redirect_to_navodila("priceska_ustrezna", {t_g: this.tipkovnica_glas});
				}
				break;

			// Ukaz konec striženja
			case "ukaz_konec_strizenja":
				this.redirect_to_navodila("strizenje_koncano_potrditev", {t_g: this.tipkovnica_glas});
				break;

			// Potrditev da je konc striženja
			case "ukaz_konec_strizenja_potrditev":
				let element_body_1 = document.getElementsByTagName("BODY")[0];
				element_body_1.onkeydown = null;

				if(payload.izbrana_akcija === "ja" || payload.izbrana_akcija === "da") {
					element_body_1.style.backgroundImage = 'url("none")';
					this.redirect_to_navodila("strizenje_koncano", {t_g: this.tipkovnica_glas});
				} else {
					this.redirect_to_navodila("priceska_ustrezna", {t_g: this.tipkovnica_glas});
				}
				break;

			// Dobimo navodilo za sliko in jo zajamemo
			case "poslusaj_slika_potem_ukaz":
				this.sendSocketNotification("zajemi_sliko_potem", {name: this.stevilka_stranke, stevec: this.stevec});
				break;

			// Potrditev ali repeat za sliko after
			case "potrditev_slika_potem":
				let element_body_2 = document.getElementsByTagName("BODY")[0];
				element_body_2.onkeydown = null;

				if(payload.izbrana_akcija === "naprej") {
					// Shranimo sliko after za kasnješo uporabo pri naslednjem striženuj..slika after v "Bazi"
					this.sendSocketNotification("shrani_sliko_after", {stranka: this.stranka_name, slika: this.slika_potem});

					// Kreiramo animacije GIF za prikaz
					element_body_2.style.backgroundImage = 'url("none")';
					this.sendSocketNotification("kreiraj_animacije", {stevilka_stranke: this.stevilka_stranke,
						image_before_name: this.slika_prej,
						image_after_name: this.slika_potem,
						navidezna_slika: this.slika_navidezna_priceska});

				} else if(payload.izbrana_akcija === "ponovi") {
					element_body_2.style.backgroundImage = 'url("none")';
					this.redirect_to_navodila("ponovno_zajem_after", {t_g: this.tipkovnica_glas});
				}
				break;

			// Prikazemo prvi gif po navodilu
			case "prikaz_prvage_gifa":
                this.star_counter = 0;
				this.redirect_to_navodila("prej_navidezna_gif", {t_g: this.tipkovnica_glas});
				break;

			// From prvi gif to drugi, tretji ali izhod
			case "from_prvi_gif":
				let element_body_3 = document.getElementsByTagName("BODY")[0];
				element_body_3.onkeydown = null;

				if(payload.izbrana_akcija === "desno") {
					element_body_3.style.backgroundImage = 'url("none")';
					this.redirect_to_navodila("prej_navidezna_potem_gif", {t_g: this.tipkovnica_glas});

				} else if(payload.izbrana_akcija === "levo") {
					element_body_3.style.backgroundImage = 'url("none")';
					this.redirect_to_navodila("prej_potem_gif", {t_g: this.tipkovnica_glas});

				} else {
					element_body_3.style.backgroundImage = 'url("none")';
					this.sendNotification("MODULE_TOGGLE", {hide: ["qr_code"], show: [], toogle:[]});
					this.redirect_to_navodila("ocenjevanje", {t_g: this.tipkovnica_glas});
				}
				break;

			// From drugi to prvi, tretji ali izhod
			case "from_drugi_gif":
				let element_body_4 = document.getElementsByTagName("BODY")[0];
				element_body_4.onkeydown = null;

				if(payload.izbrana_akcija === "desno") {
					element_body_4.style.backgroundImage = 'url("none")';
					this.redirect_to_navodila("prej_potem_gif", {t_g: this.tipkovnica_glas});

				} else if(payload.izbrana_akcija === "levo") {
					element_body_4.style.backgroundImage = 'url("none")';
					this.redirect_to_navodila("prej_navidezna_gif", {t_g: this.tipkovnica_glas});

				} else {
					element_body_4.style.backgroundImage = 'url("none")';
					this.sendNotification("MODULE_TOGGLE", {hide: ["qr_code"], show: [], toogle:[]});
					this.redirect_to_navodila("ocenjevanje", {t_g: this.tipkovnica_glas});
				}
				break;

			// From tretji gif to prvi, drugi, izhod
			case "from_tretji_gif":
				let element_body_5 = document.getElementsByTagName("BODY")[0];
				element_body_5.onkeydown = null;

				if(payload.izbrana_akcija === "desno") {
					element_body_5.style.backgroundImage = 'url("none")';
					this.redirect_to_navodila("prej_navidezna_gif", {t_g: this.tipkovnica_glas});

				} else if(payload.izbrana_akcija === "levo") {
					element_body_5.style.backgroundImage = 'url("none")';
					this.redirect_to_navodila("prej_navidezna_potem_gif", {t_g: this.tipkovnica_glas});

				} else {
					element_body_5.style.backgroundImage = 'url("none")';
					this.sendNotification("MODULE_TOGGLE", {hide: ["qr_code"], show: [], toogle:[]});
					this.redirect_to_navodila("ocenjevanje", {t_g: this.tipkovnica_glas});
				}
				break;

            case "change_stars":
                if(payload.izbrana_akcija === "nič" || payload.izbrana_akcija === "0") {
                    this.star_counter = 0;
					this.redirect_to_navodila("ocenjevanje", {t_g: this.tipkovnica_glas});
                } else if(payload.izbrana_akcija === "ena" || payload.izbrana_akcija === "1") {
                    this.star_counter = 1;
					this.redirect_to_navodila("ocenjevanje", {t_g: this.tipkovnica_glas});
                } else if(payload.izbrana_akcija === "dva" || payload.izbrana_akcija === "2") {
                    this.star_counter = 2;
					this.redirect_to_navodila("ocenjevanje", {t_g: this.tipkovnica_glas});
                } else if(payload.izbrana_akcija === "tri" || payload.izbrana_akcija === "3") {
                    this.star_counter = 3;
					this.redirect_to_navodila("ocenjevanje", {t_g: this.tipkovnica_glas});
                } else if(payload.izbrana_akcija === "štiri" || payload.izbrana_akcija === "4") {
                    this.star_counter = 4;
					this.redirect_to_navodila("ocenjevanje", {t_g: this.tipkovnica_glas});
                } else if(payload.izbrana_akcija === "pet" || payload.izbrana_akcija === "5") {
                    this.star_counter = 5;
					this.redirect_to_navodila("ocenjevanje", {t_g: this.tipkovnica_glas});
                } else if(payload.izbrana_akcija === "šest" || payload.izbrana_akcija === "6") {
                    this.star_counter = 6;
					this.redirect_to_navodila("ocenjevanje", {t_g: this.tipkovnica_glas});
                } else if(payload.izbrana_akcija === "sedem" || payload.izbrana_akcija === "7") {
                    this.star_counter = 7;
					this.redirect_to_navodila("ocenjevanje", {t_g: this.tipkovnica_glas});
                } else if(payload.izbrana_akcija === "osem" || payload.izbrana_akcija === "8") {
                    this.star_counter = 8;
					this.redirect_to_navodila("ocenjevanje", {t_g: this.tipkovnica_glas});
                } else if(payload.izbrana_akcija === "devet" || payload.izbrana_akcija === "9") {
                    this.star_counter = 9;
					this.redirect_to_navodila("ocenjevanje", {t_g: this.tipkovnica_glas});
                } else if(payload.izbrana_akcija === "deset" || payload.izbrana_akcija === "10") {
                    this.star_counter = 10;
					this.redirect_to_navodila("ocenjevanje", {t_g: this.tipkovnica_glas});
                } else if(payload.izbrana_akcija === "potrdi") {
					this.redirect_to_navodila("zakljucek", {t_g: this.tipkovnica_glas});
                }
		}
	},

	/**
	 * Funkcija preusmeri (poslje notification), ki ga razred
	 *'navodila_salon' prebere in prikaže novo navodilo
	 *
	 * @param ukaz
	 * @param data
	 */
	redirect_to_navodila: function (ukaz, data) {
		this.sendNotification(ukaz, data);
	},

	/**
	 * Funkcija default parametru 'spol' priredi vrednost
	 * M (moški) ali F(ženska)
	 *
	 * @param izbira
	 */
	gender_selection: function(izbira) {
		this.spol = izbira;
	},

	/**
	 * Funkcija default parametru 'dolzina' priredi vrednost
	 * L (long) ali S (short)
	 *
	 * @param izbira
	 */
	length_selection: function (izbira) {
		this.dolzina = izbira;
	},

	/**
	 * Funkcija default parametru 'barva' priredi vrednost
	 * L (light), D (dark), B (brown) ali Č (črna)
	 *
	 * @param izbira
	 */
	color_selection: function (izbira) {
		this.barva = izbira;
	}
});