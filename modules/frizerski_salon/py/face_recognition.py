# Recognizing a face of person in front of the camera.
# Run: ./python3 face_recognition.py
import cv2
import numpy
import os

# File that contains specific parameters for face recognition
# Source:
# https://github.com/vschs007/flask-realtime-face-detection-opencv-python/blob/master/haarcascade_frontalface_default.xml
haar_file = 'haarcascade_frontalface_default.xml'

# Folder where photo examples of persons are stored
dataset = 'dataset'

# Create a list of images and a list of corresponding names
# labels are number from 0 - n (for each user one number) - example: '0' = jernej, "1" = miha
# images are stored from 0 - n (each has its own label)
(images, lables, names, id) = ([], [], {}, 0)
for(subdirs, dirs, files) in os.walk(dataset):
    for subdir in dirs:
        names[id] = subdir
        subject_path = os.path.join(dataset, subdir)
        for filename in os.listdir(subject_path):
            path = subject_path + '/' + filename
            lable = id
            images.append(cv2.imread(path, 0))
            lables.append(int(lable))
        id += 1
(width, height) = (130, 100)

# Train model from images in dataset
(images, lables) = [numpy.array(lis) for lis in [images, lables]]
model = cv2.face.LBPHFaceRecognizer_create()
model.train(images, lables)

# Set up camera
face_cascade = cv2.CascadeClassifier(haar_file)
webcam = cv2.VideoCapture(-1)

# Predict face of the person sitting in front of the camera (and show possibility)
while True:
    (_, im) = webcam.read()
    gray = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
    faces = face_cascade.detectMultiScale(gray, 1.3, 5)

    for (x, y, w, h) in faces:
        cv2.rectangle(im, (x, y), (x + w, y + h), (255, 0, 0), 2)
        face = gray[y:y + h, x:x + w]
        face_resize = cv2.resize(face, (width, height))
        # Try to recognize the face
        prediction = model.predict(face_resize)
        cv2.rectangle(im, (x, y), (x + w, y + h), (0, 255, 0), 3)

        if prediction[1] < 500:

            cv2.putText(im, '% s - %.0f' %
                        (names[prediction[0]], prediction[1]), (x - 10, y - 10),
                        cv2.FONT_HERSHEY_PLAIN, 1, (0, 255, 0))
        else:
            cv2.putText(im, 'not recognized',
                        (x - 10, y - 10), cv2.FONT_HERSHEY_PLAIN, 1, (0, 255, 0))

        cv2.imshow('OpenCV', im)

        key = cv2.waitKey(10)
        if key == 27:
            break
