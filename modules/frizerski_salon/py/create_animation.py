import shutil
import Image
import ImageDraw
import ImageFont
import cv2
import os
import sys

image_folder = sys.argv[1]
animation_path = sys.argv[2]
path_to_image_before = sys.argv[3]
path_to_image_after = sys.argv[4]
path_to_image_navidezne = sys.argv[5]
image_before_name = sys.argv[6]
image_after_name = sys.argv[7]
image_navidezna_name = sys.argv[8]
font_ttf = sys.argv[9]

# Odpremo direktorij kamor bomo shranjevali animacije (npr. :/animation/jernej/...)
pathToAnimations = animation_path + "/" + image_folder
if os.path.isdir(pathToAnimations):
    shutil.rmtree(pathToAnimations)
os.mkdir(pathToAnimations)

image_before = Image.open(path_to_image_before + "/" + image_before_name)
image_navidezna = Image.open(path_to_image_navidezne + "/" + image_folder + "/" + image_navidezna_name)
image_after = Image.open(path_to_image_after + "/" + image_after_name)

drawBefore = ImageDraw.Draw(image_before)
drawAfter = ImageDraw.Draw(image_after)
drawNavidezna = ImageDraw.Draw(image_navidezna)

font = ImageFont.truetype(font=font_ttf, size=24)

drawBefore.text((30, 10), "PRED STRIZENJEM", (255, 255, 255), font=font)
drawAfter.text((30, 10), "PO STRIZENJU", (255, 255, 255), font=font)
drawNavidezna.text((30, 10), "PRED STRIZENJEM Z NAVIDEZNO PRICESKO", (255, 255, 255), font=font)

# PREJ VS PREJ Z NAVIDEZNO
images = []
images.append(image_before)
images.append(image_navidezna)
images[0].save(pathToAnimations + "/" + 'prej_navidezna.gif',
               save_all=True,
               append_images=images[1:],
               duration=500,
               loop=0)

# PREJ VS POTEM
images = []
images.append(image_before)
images.append(image_after)
images[0].save(pathToAnimations + "/" + 'prej_potem.gif',
               save_all=True,
               append_images=images[1:],
               duration=500,
               loop=0)

# PREJ VS PREJ Z NAVIDEZNO VS POTEM
images = []
images.append(image_before)
images.append(image_navidezna)
images.append(image_after)
images[0].save(pathToAnimations + "/" + 'prej_navidezna_potem.gif',
               save_all=True,
               append_images=images[1:],
               duration=500,
               loop=0)
print("gifs_created")
print(image_folder)
