import time
import cv2
import sys
from os import chdir

image_name = sys.argv[1]
image_path = sys.argv[3]
haar_file = sys.argv[4]
eyes_haar_file = sys.argv[5]

chdir(sys.argv[2]) if sys.argv[2] else sys.exit()

face_cascade = cv2.CascadeClassifier(haar_file)
eye_cascade = cv2.CascadeClassifier(eyes_haar_file)
webcam = cv2.VideoCapture(-1)

while True:
    (_, im) = webcam.read()
    gray = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
    faces = face_cascade.detectMultiScale(gray, 1.3, 5)

    cv2.rectangle(im, (160, 90), (400, 370), (0, 255, 0), 3)
    cv2.rectangle(im, (230, 180), (280, 220), (255, 255, 255), 3)
    cv2.rectangle(im, (300, 180), (350, 220), (255, 255, 255), 3)
    cv2.imshow('Postavitev obraza za sliko', im)
    key = cv2.waitKey(1)

    if key == 13:
        if len(faces) > 0:
            # Vzamemo sam prvo faco
            face = faces[0]
            faceX = face[0]
            faceY = face[1]
            faceW = face[2]
            faceH = face[3]

            roi_gray = gray[faceY:faceY + faceH, faceX:faceX + faceW]
            roi_color = im[faceY:faceY + faceH, faceX:faceX + faceW]
            eyes = eye_cascade.detectMultiScale(roi_gray)

            if len(eyes) >= 2:
                eye_1 = eyes[0]
                eye_1_y = eye_1[1]
                eye_2 = eyes[1]
                eye_2_y = eye_2[1]

                time.sleep(0.1)
                return_value, image = webcam.read()
                cv2.imwrite("image/" + image_name + ".jpg", image)
                print("taken")
                break

