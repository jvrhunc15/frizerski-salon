import speech_recognition as sr
import cv2
import sys

# import sounddevice as sd
# print(sd.query_devices())

frequency = 2500  # Set Frequency To 2500 Hertz
duration = 1000  # Set Duration To 1000 ms == 1 second

img = cv2.imread(sys.argv[1])
height, width = img.shape[:2]
res = cv2.resize(img, (int(width/3), int(height/3)), interpolation=cv2.INTER_CUBIC)
winname = "Klikni na 'Enter' in spregovori"
cv2.namedWindow(winname)
cv2.moveWindow(winname, 1500, 200)

cv2.imshow(winname, res)
key = cv2.waitKey(0)
if key == 13:
    cv2.destroyAllWindows()

recognizer = sr.Recognizer()
microphone = sr.Microphone(device_index=3)

# Izpise vse mikrofone
# for index, name in enumerate(microphone.list_microphone_names()):
#    print("Microphone with name \"{1}\" found for `Microphone(device_index={0})`".format(index, name))

if not isinstance(recognizer, sr.Recognizer):
    raise TypeError("`recognizer` must be `Recognizer` instance")

if not isinstance(microphone, sr.Microphone):
    raise TypeError("`microphone` must be `Microphone` instance")

recognizer.dynamic_energy_threshold = False
recognizer.energy_threshold = 3000
recognizer.pause_threshold = 0.9

with microphone as source:
    recognizer.adjust_for_ambient_noise(source, duration=0)
    audio = recognizer.listen(source)

    try:
        speech_text = recognizer.recognize_google(audio, language="sl-SI")
        print(speech_text)
    except sr.UnknownValueError:
        print("napaka")
    except sr.RequestError as e:
        print("napaka")
