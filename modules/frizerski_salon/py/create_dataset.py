# Creating database for selected person.
# Example for execution: ./python3 create_dataset.py jernej
import cv2
import os
import sys

# File that contains specific parameters for face recognition
# Source:
# https://github.com/vschs007/flask-realtime-face-detection-opencv-python/blob/master/haarcascade_frontalface_default.xml
haar_file = 'haarcascade_frontalface_default.xml'

# All sub folders for specific person will be in this dir
dataset = 'dataset'

# Name of the person is given by system parameter
sub_data = sys.argv[1]

# Join path to the sub folder if it exists otherwise create new sub folder
path = os.path.join(dataset, sub_data)
if not os.path.isdir(path):
    os.mkdir(path)

# Set constant width and height for pictures
(width, height) = (130, 100)

# Set up a webcam -> I have a integrated webcam (-1)
face_cascade = cv2.CascadeClassifier(haar_file)
webcam = cv2.VideoCapture(-1)

# Create 30 photos of a persons face
count = 1
while count < 101:
    (_, im) = webcam.read()
    gray = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
    faces = face_cascade.detectMultiScale(gray, 1.3, 4)
    for (x, y, w, h) in faces:
        cv2.rectangle(im, (x, y), (x + w, y + h), (255, 0, 0), 2)
        face = gray[y:y + h, x:x + w]
        face_resize = cv2.resize(face, (width, height))
        cv2.imwrite('% s/% s.png' % (path, count), face_resize)
    count += 1

    cv2.imshow('OpenCV', im)
    key = cv2.waitKey(10)
    if key == 27:
        break
