import qrcode
import sys

qr = qrcode.QRCode(version=1, box_size=10, border=4)

image_name = sys.argv[1]
stranka_name = sys.argv[2]
qr_codes_path = sys.argv[3]

data = 'http://localhost:8080/image?stranka=' + stranka_name + '&slika=' + image_name
qr.add_data(data)
qr.make(fit=True)
img = qr.make_image(fill='black', back_color='white')
img.save(qr_codes_path + "/qr_" + image_name)
print("qr_" + image_name)
