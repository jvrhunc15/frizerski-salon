import qrcode
import sys

gif_folder = sys.argv[1]
gif_folder_path = sys.argv[2]
animations_save_path = sys.argv[3]

for i in range(3):
    # prej navidezna
    if(i == 0):
        qr = qrcode.QRCode(version=1, box_size=10, border=4)
        data = 'http://localhost:8080/gif?stranka_id=' + gif_folder + '&gif_name=' + "prej_navidezna.gif"
        qr.add_data(data)
        qr.make(fit=True)
        img = qr.make_image(fill='black', back_color='white')
        img.save(animations_save_path + "/" + gif_folder + "/prej_navidezna_qr.jpg")

    # prej navidezna potem
    elif i == 1:
        qr1 = qrcode.QRCode(version=1, box_size=10, border=4)
        data1 = 'http://localhost:8080/gif?stranka_id=' + gif_folder + '&gif_name=' + "prej_potem_navidezna.gif"
        qr1.add_data(data1)
        qr1.make(fit=True)
        img = qr1.make_image(fill='black', back_color='white')
        img.save(animations_save_path + "/" + gif_folder + "/prej_navidezna_potem_qr.jpg")

    # prej potem
    else:
        qr2 = qrcode.QRCode(version=1, box_size=10, border=4)
        data2 = 'http://localhost:8080/gif?stranka_id=' + gif_folder + '&gif_name=' + "prej_potem.gif"
        qr2.add_data(data2)
        qr2.make(fit=True)
        img = qr2.make_image(fill='black', back_color='white')
        img.save(animations_save_path + "/" + gif_folder + "/prej_potem_qr.jpg")


print("qr_gif_created")
print(gif_folder)
