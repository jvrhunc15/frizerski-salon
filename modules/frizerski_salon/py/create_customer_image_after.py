import time
import cv2
import sys
from os import chdir

image_name = sys.argv[1]
image_path = sys.argv[3]
haar_file = sys.argv[4]
eyes_haar_file = sys.argv[5]

chdir(sys.argv[2]) if sys.argv[2] else sys.exit()

camera = cv2.VideoCapture(-1)
time.sleep(0.1)
return_value, image = camera.read()
cv2.imwrite("image_after/" + image_name + ".jpg", image)
del camera
print('taken')


