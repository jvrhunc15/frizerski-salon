# Recognizing a face of person in a picure.
# Run: ./python3 face_recognition.py image/1.jpg dataset
import cv2
import numpy
import os
import sys

# Path to image
image_path = sys.argv[1]

# Folder where photo examples of persons are stored
dataset = sys.argv[2]

# File that contains specific parameters for face recognition
# Source:
# https://github.com/vschs007/flask-realtime-face-detection-opencv-python/blob/master/haarcascade_frontalface_default.xml
haar_file = sys.argv[3]

# Create a list of images and a list of corresponding names
# labels are number from 0 - n (for each user one number) - example: '0' = jernej, "1" = miha
# images are stored from 0 - n (each has its own label)
(images, lables, names, id) = ([], [], {}, 0)
for(subdirs, dirs, files) in os.walk(dataset):
    for subdir in dirs:
        names[id] = subdir
        subject_path = os.path.join(dataset, subdir)
        for filename in os.listdir(subject_path):
            path = subject_path + '/' + filename
            lable = id
            images.append(cv2.imread(path, 0))
            lables.append(int(lable))
        id += 1
(width, height) = (130, 100)

# Train model from images in dataset
(images, lables) = [numpy.array(lis) for lis in [images, lables]]
model = cv2.face.LBPHFaceRecognizer_create()
model.train(images, lables)

# Set up imporatant things
image = cv2.imread(image_path)
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
faceCascade = cv2.CascadeClassifier(haar_file)
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
faces = faceCascade.detectMultiScale(gray, 1.3, 5)

# Predict for faces on camera
for (x, y, w, h) in faces:
    cv2.rectangle(image, (x, y), (x + w, y + h), (255, 0, 0), 2)
    face = gray[y:y + h, x:x + w]
    face_resize = cv2.resize(face, (width, height))
    prediction = model.predict(face_resize)
    cv2.rectangle(image, (x, y), (x + w, y + h), (0, 255, 0), 3)

    if prediction[1] >= 60:

        cv2.putText(image, '% s - %.0f' %
                    (names[prediction[0]], prediction[1]), (x - 10, y - 10),
                    cv2.FONT_HERSHEY_PLAIN, 1, (0, 255, 0))
        print('%s,%0.f' % (names[prediction[0]], prediction[1]))

    else:
        cv2.putText(image, 'not recognized',
                    (x - 10, y - 10), cv2.FONT_HERSHEY_PLAIN, 1, (0, 255, 0))
        print('unknown')


# cv2.imshow("Faces found", image)
# cv2.waitKey(0)
