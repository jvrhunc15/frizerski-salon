import shutil
import Image
import cv2
import os
import sys

image_path = sys.argv[1]
image_name = sys.argv[2]
spol = sys.argv[3]
dolzina = sys.argv[4]
barva = sys.argv[5]
haar_file = sys.argv[6]
eyes_file = sys.argv[7]
haircuts_path = sys.argv[8]
resized_haircuts_path = sys.argv[9]
final_image_save_path = sys.argv[10]

# Odpremo sliko na kateri bomo prikazovali vse priceske (npr. 1.jpg, 2.jpg itd.)
# x,y,w,h so parametri kjer je prepoznan obraz stranke (na levi)
faceCascade = cv2.CascadeClassifier(haar_file)
eye_cascade = cv2.CascadeClassifier(eyes_file)
image = cv2.imread(image_path)
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
faces = faceCascade.detectMultiScale(
    gray,
    scaleFactor=1.3,
    minNeighbors=5
)
customer_face = faces[0]
x = customer_face[0]
y = customer_face[1]
w = customer_face[2]
h = customer_face[3]

# cv2.rectangle(image, (x, y), (x + w, y + h), (0, 255, 0), 3)
# cv2.imshow("Faces found", image)
# cv2.waitKey(0)

roi_gray = gray[y:y + h, x:x + w]
roi_color = image[y:y + h, x:x + w]
eyes = eye_cascade.detectMultiScale(roi_gray)

# Eye 1
eye1 = eyes[0]
e1x = eye1[0]
e1y = eye1[1]
e1w = eye1[2]
e1h = eye1[3]

# Eye 2
eye2 = eyes[1]
e2x = eye2[0]
e2y = eye2[1]
e2w = eye2[2]
e2h = eye2[3]


# TODO nevem zakaj tocno, ampak če delim razdaljo od desno oko - desni rob,
#  ter levo oko - levi rob dobim natančno koliko rabim, da pride slika na
#  svoje mest
delitelj = 9
# delitelj = abs(int(e1x) - int(e2x)) / 6

# Uravnamo širino in začeten x (glede na oči)
original_width = w
width_adjust = int((e1x / delitelj) + ((w - e2x) / delitelj))
w -= int(width_adjust)

# e2x je leva učka na sliki
if (e2x < e1x):
    x += int(e2x / delitelj)
# e1x je desna učka na sliki
else:
    x += int(e1x / delitelj)

if(w > 250):
    x += 12
elif(250 > w >= 200):
    x += 9
else:
    x += 6


# dolzina med ocema
# print("dolzina: %0.f" % abs(e1x - e2x))

# Za kolk mormo nakoncu frizuro prestavit gor da bo direkt na čelo
y_Corelation = int((e2y + e1y) / 2)

# Odpremo direktorij kamor bomo shranjevali datoteke koncne pravilno obrezane slike
savePath = resized_haircuts_path + "/" + image_name
if os.path.isdir(savePath):
    shutil.rmtree(savePath)
os.mkdir(savePath)

# Odpremo dir kamor bomo shranjevali koncne slike
saveImagePath = final_image_save_path + "/" + image_name
if os.path.isdir(saveImagePath):
    shutil.rmtree(saveImagePath)
os.mkdir(saveImagePath)

# Nastavimo pot do pravih pričesk (glede na spol)
if spol == 'M':
    haircuts_path = haircuts_path + "/male"
else:
    haircuts_path = haircuts_path + "/female"

# Nastavimo pot do pravih pričesk (glede na dolzino)
if dolzina == 'S':
    haircuts_path = haircuts_path + "/short"
else:
    haircuts_path = haircuts_path + "/long"

# Nastavimo pot do pravih pričesk (glede na barvo)
if barva == 'L':
    haircuts_path = haircuts_path + "/light"
else:
    haircuts_path = haircuts_path + "/dark"

# Vsako frizuro med izbranimi resizamo,
# da bo primerna za uporabo na naši sliki
# (Vsako resizamo na width, ki je enaka našemu okvirju obraza)
# (Potem postavimo face width v naš okvir in ostalo pustimo zunaj)
haircuts = os.listdir(haircuts_path)
for haircut in haircuts:
    haircut_name = haircut.replace('.png', '')

    # Splitamo, da dobimo podatke koliko prostora zaseda na originalni sliki
    # leva, desna in face stran obraza
    haircut_name_split = haircut_name.split('_')
    name = image_name + "_" + haircut_name_split[0]
    left_width = int(haircut_name_split[1])
    face_width = int(haircut_name_split[2])
    right_width = int(haircut_name_split[3])
    up_height = int(haircut_name_split[4])

    resized_left_width = round(((left_width * w) / face_width), 3)
    resized_right_width = round(((right_width * w) / face_width), 3)
    resized_face_width = round(w, 3)

    full_resized_width = resized_left_width + resized_right_width + resized_face_width

    basewidth = int(full_resized_width)
    image = Image.open(haircuts_path + "/" + haircut)
    wpercent = (basewidth / float(image.size[0]))
    hsize = int((float(image.size[1]) * float(wpercent)))

    resized_up_height = round((hsize * up_height) / h, 3)

    image = image.resize((basewidth, hsize), Image.ANTIALIAS)
    image.save(savePath + "/" + name + "_" + str(resized_left_width) + "_" + str(resized_face_width) + "_" + str(
        resized_right_width) + "_" + str(resized_up_height) + ".png")

# Vse resized pričeske dodamo na originalno sliko, in to sliko shranimo
resized_haircuts = os.listdir(savePath)
for r_haircut in resized_haircuts:
    background = Image.open(image_path)
    foreground = Image.open(savePath + "/" + r_haircut)

    left_width = r_haircut.split('_')[2]
    up_height = r_haircut.split('_')[5].replace('.png', '')


    background.paste(foreground, (x - int(float(left_width)), y - int(float(up_height)) - y_Corelation), foreground.convert("RGBA"))
    background.save(saveImagePath + "/" + r_haircut)

# background.show()

print(image_name)
