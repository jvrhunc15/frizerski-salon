import time
import cv2
import sys
from os import chdir

image_name = sys.argv[1]
image_path = sys.argv[3]
haar_file = sys.argv[4]
eyes_haar_file = sys.argv[5]

chdir(sys.argv[2]) if sys.argv[2] else sys.exit()


camera = cv2.VideoCapture(-1)
time.sleep(0.1)
return_value, image = camera.read()
cv2.imwrite("image/" + image_name + ".jpg", image)

del camera

faceCascade = cv2.CascadeClassifier(haar_file)
eye_cascade = cv2.CascadeClassifier(eyes_haar_file)
image = cv2.imread(image_path)
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
faces = faceCascade.detectMultiScale(
    gray,
    scaleFactor=1.3,
    minNeighbors=5
)

if len(faces) < 1:
    print('napaka')
    exit(0)

customer_face = faces[0]
x = customer_face[0]
y = customer_face[1]
w = customer_face[2]
h = customer_face[3]

# cv2.rectangle(image, (x, y), (x + w, y + h), (0, 255, 0), 3)
# cv2.imshow("Faces found", image)
# cv2.waitKey(0)

roi_gray = gray[y:y + h, x:x + w]
roi_color = image[y:y + h, x:x + w]
eyes = eye_cascade.detectMultiScale(roi_gray)

if len(eyes) < 2:
    print("napaka")
    exit(0)

# Eye 1
eye1 = eyes[0]
e1x = eye1[0]
e1y = eye1[1]
e1w = eye1[2]
e1h = eye1[3]

# Eye 2
eye2 = eyes[1]
e2x = eye2[0]
e2y = eye2[1]
e2w = eye2[2]
e2h = eye2[3]

# Ce smo dost ravni je uredu slika, drgč mormo še enkrat slikat
if abs(e2y - e1y) > 25:
    print("napaka")
else:
    print('taken')


